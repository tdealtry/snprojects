# Session 8
[[_TOC_]]

## Fitting a function to a histogram/graph

Fitting a `TH1F` and a `TGraph` have (almost) the same options, so I'm going to just show the example of doing one.

I'd recommend checking the ROOT documentation, as there are **lots** of fitting options. You'll want to decide what you require that best suits your needs
- [`TH1::Fit()`](https://root.cern.ch/doc/master/classTH1.html#a63eb028df86bc86c8e20c989eb23fb2a)
- [`TGraph::Fit()`](https://root.cern.ch/doc/master/classTGraph.html#a61269bcd47a57296f0f1d57ceff8feeb)

Note that there are two `Fit()` functions for each - you'll want to check the documentation for both, as not everything is described in both.

I'll do a simple example, not using SN data
```C++
//Define a TGraph with 100 points
TGraph * g = new TGraph(100);

//Create a random number generator, so that we can use it to fit
// The argument is the seed. If it is positive (and non-zero) it is repeatable
TRandom3 r(200);

//Fill the TGraph
for(int ip = 0; ip < 100; ip++) {
	// We do y = x, with some flat randomness on y with half-width 5
	//  See https://root.cern.ch/doc/master/classTRandom3.html
	//  for more random functions you can pull from
	//   (e.g. exponential with Exp(), Gaussian with Gaus(), Poission with Poission(), ...)
	g->SetPoint(ip, ip, ip + r.Uniform(-5, +5));
}//ip

//Fit a straight line
g->Fit("pol1");

//And finally draw
g->Draw("ALP");
```
After calling `Fit()`, cehck the terminal. What do you see?

You can now have a play with other in-built fitting functions in the same session e.g.
* Quadratic: `g->Fit("pol2");`
* Cubic: `g->Fit("pol3");`
* Gaussian: `g->Fit("gaus");`
* Expoential: `g->Fit("expo");`

### Fitting over only part of the histogram/graph range
Use the 4th & 5th arguments of `Fit()` to define the range you want to fit over
```C++
g->Fit("pol1", "", "", 10, 50);
```

### Defining your own function to fit
This is a 2 step process
- Create an instance of the ROOT function class `TF1`, that has the formula you want to fit
- Pass this formula to `Fit()`

Again there are lots of options, so check the [ROOT documentation on `TF1`](https://root.cern.ch/doc/master/classTF1.html) for how to do what you need.

This is my [favoured way for simple functions](https://root.cern.ch/doc/master/classTF1.html#F2).
Using the same data as above...
```C++
//define a straight line function
// You're essentially doing the right hand side of the equation y = ?, where you fill in the ? with what you want
// The square brackets define the parameters that are free to move

TF1 * func1 = new TF1("func1", "[0]+x*[1]");

//Use it to fit
g->Fit(func1);
```

We can define more complex things like
```C++
TF1 * func2 = new TF1("func2", "[0]+sin(x)*[1]-4/(pow(x,4))");
g->Fit(func2, "", "", 50, 70);
```

### Initial value of fit parameters
Sometimes the fitter fails. This is more likely if the fit function doesn't describe the data very well, but can happen even when it *should* work.

In order to help the fitter find the best fit, it can be useful to initialise the parameter values with `SetParameter()` before calling `Fit()`
```C++
func1->SetParameter(0, 0.01);
func1->SetParameter(1, 1.5);
func1->SetLineColor(kBlue);
func1->Draw("SAME");
```
You can see the function is set to a straight line that doesn't pass through the points
```C++
g->Fit(func1);
```
You can now see the best fit passes through all points

### Has the fit worked?
`Fit()` returns a 
- `= 0` means no error
- `> 0` means an error from the minimiser
- `< 0` means an error from something else

You'll want to check the result, to check it worked
```
int status = g->Fit(func1);
std::cout << "Fit status: " << status << std::endl;
```

You'll also want to check how good the fit is. e.g. if you're performing a chisq fit, compare the chisq with the number of degrees of freedom

### Getting the fit result programatically
The fit result is
- printed to screen,
- can be available in the stats box

but you sometimes also need access to the result programatically.

If you've used a `TF1` defined by yourself, you want to use `GetParameter()`
```C++
for(int ip = 0; ip < func1->GetNpar(); ip++) {
	std::cout << "Parameter " << ip << " = " << func1->GetParameter(ip) << std::endl;
}//ip
```

Alternatively, you can use the return value of `Fit()`, and pass `"S"` as the second argument
```C++
TFitResultPtr result = g->Fit(func1,"S");
for(int ip = 0; ip < result.Get()->NPar(); ip++) {
	std::cout << "Parameter " << ip << " = " << result.Get()->Parameter(ip) << std::endl;
}//ip

//You can still access the fit status
std::cout << "Fit status: " << result.Get()->Status() << std::endl;
```

## Stacking histograms
Say you want to see the distribution for some variable, broken down by some other variable. One way of visualising this is by statcking histograms on top of each other, so you can see both the relative fraction of each *other variable*, and the total distribution

To do this, we use a `THStack` (i.e. a histogram stack). 

Take a look in the example `stack_example.C`, which plots the true neutrino energy distribution, broken down by interacting neutrino flavour

