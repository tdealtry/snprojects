# Session 2
[[_TOC_]]

## More on plotting a `TTree` with ROOT
Last time, we saw that
- We can plot simple histograms (1D, no cuts) with a `TBrowser`
- We also learnt how to do the same thing from the command line e.g. `reconTree->Draw("Energy");`
- And how to draw 2D distributions `reconTree->Draw("Vertex[0]:Vertex[1]")`

### Styling the histogram
- The default 2D histogram is drawn in such a way that it's hard to interpret
  - Dots randomly distributed in histogram bins that are not defined
- A more useful 2D plot is the `COLZ`
- How do we get ROOT to plot like this?
  - You can play with options. But this is slow to do every time...
  - Instead can give `TTree::Draw()` an extra option
	- Type `reconTree->Draw(` then `<TAB>` a couple of times
	- See the third argument, called `option`, which takes an `Option_t *` (basically a string)
	- Can then modify the draw call to be `reconTree->Draw("Vertex[0]:Vertex[1]","","COLZ")`, being careful to give the second argument (`selection`) its default argument (an empty `const char *` (again, basically a string))

### Applying cuts
- Triggering on data does two things
  - Finding the interesting pieces of data (e.g. neutrino interactions)
  - Rejecting uninteresting pieces of data (e.g. PMT dark noise)
- Triggering is not 100% efficient, so we have some amount of background contamination in our physics signal (we also lose some physics at low energies, below ~10 MeV)
- Therefore we want to do things after triggering to do more rejection of the background, to enhance the signal/background ratio
- In order to do this, we can use the `selection` argument of `TTree::Draw()`
  - e.g. `reconTree->Draw("Time","Energy>7")`
  
### Drawing multiple histograms
It can be useful to draw multiple histograms on the same axes, for comparison.
To do this, there is a histogram *styling* option called `"SAME"`

For example, to draw the time distribution with & without the energy cut
```C++
reconTree->Draw("Time")`
reconTree->Draw("Time","Energy>7","SAME")
```

### Further reading
- [TTree::Draw()](https://root.cern.ch/doc/master/classTTree.html#a73450649dc6e54b5b94516c468523e45)
- [THistPainter](https://root.cern.ch/doc/master/classTHistPainter.html) for style options

## ROOT analysis macros
It's nice to play in the ROOT command line to learn new things & try things out, but to really do analysis properly you need to write code in files so it is repeatable (so you can check what was run) and quickly runnable (you don't want to type the same thing out lots of times! Especially if you put a typo in)

To do this, we write C++ `.C` files

### `TChain`ing files together
There is an example `chain.C` file in this repo, that defines a function `chainit()` that
- Takes a file path and `TTree` name as arguments
- Creates a `TChain`
- Adds the files to it

Why do we need this? To be able to see to whole SN in one

Load the macro like this (it then allows you to use the functions defined inside)
```C++
.L chain.C+ //loads the chain.C macro, so its functions can be used
```
Note the `+` at the of the filename. This makes ROOT do compilation of the file *properly*, which means code runs faster & you get better debugging help

Then to use the function defined
```C++
TChain * t = chainit("/data/tdealtry/2021summerprod/sntools/v1.0.1/nakazato/NO_10kpc_2001/HyperK_20perCent/0000/wcsim/v1.9.4_C/triggerapp/v1.1.2_bonsaiv1.1.1_flowerv1.1_A/flbo/*.root")
```

You then can use the `TChain` just like a `TTree`, e.g. drawing things like
```C++
t->Draw("Vertex[0]:Vertex[1]","Energy>7","COLZ");
```
Note that you use `t` rather than `reconTree`, since that's how the `TChain *` was named above. You can use (almost) anything instead of `t` if it is clearer for you


## Next step
The next step is to compare different SN from the same configuration. E.g. to see the statistical uncertainty on the mean reconstructed energy
