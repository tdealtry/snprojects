#include "chain.C"
#include "TH1F.h"
#include "TPad.h"
#include "TString.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TMath.h"
#include <sys/stat.h>

TH1F * spread(const int nbins = 20,
	      const int xmin = 10,
	      const int xmax = 50,
	      const char * variable = "Energy",
	      const char * cuts = "",
	      const char * title = "",
	      const char * xaxis_title = "Reconstructed Energy (MeV)",
	      const char * yaxis_title = "Number of entries",
	      const char * model = "nakazato",
	      const char * mass_ordering = "NO",
	      const char * distance = "10",
	      const char * progenitor = "2001")
{
  //create an array of vectors to store the results
  // The array is nbins long (i.e. has one array entry)
  // Each vector inside is iiter long (i.e. stores one entry for the each SN)
  vector<double> results[nbins];
  
  //and create a TH1F (histogram), such that we can use it to fill the output information
  TH1F * histo = new TH1F(variable, TString::Format("%s;%s;%s", title, xaxis_title, yaxis_title), nbins, xmin, xmax);
	
  //loop over 100 iterations of the same configuration of input files
  for(int iiter = 0; iiter < 100; iiter++){
	  
    //generate the file path
    // the %04d is replaced by an int (%d), padded with 0s to 4 digits (04), to match the actual directory structure
    auto reco_file_path = TString::Format("/data/tdealtry/2021summerprod/sntools/v1.0.1/%s/%s_%skpc_%s/HyperK_20perCent/%04d/wcsim/v1.9.4_C/triggerapp/v1.1.2_bonsaiv1.1.1_flowerv1.1_A/flbo/", model, mass_ordering, distance, progenitor, iiter);
	  
    //check the directory exists
    struct stat exists;
    if(stat(reco_file_path, &exists) != 0) {
      std::cout << "Directory does not exist: " << reco_file_path << " Continuing..." << std::endl;
      continue;
    }
    std::cout << "Opening files in flbo reconstructed directory: " << reco_file_path << std::endl;

    //get the TChain = all files from this configuration
    TChain * treco = chainit(reco_file_path + "*.root", "reconTree");

    //Get the data we want
    //First draw
    treco->Draw(TString::Format("%s>>myhtemp(%d,%d,%d)", variable, nbins, xmin, xmax), cuts);
    //Get the resulting histogram
    auto htemp = (TH1F*)gPad->GetPrimitive("myhtemp"); //1D

    std::cout << htemp << std::endl;

    //and fill the results.
    //Recall that the array has one entry for each bin, and each entry in the vector is for the current SN
    for(int ix = 1; ix <= htemp->GetNbinsX(); ix++) {
      //results[ix - 1] because the array index runs from 0 to nbins-1
      //GetBinContent(ix) because histogram bins run from 1 to nbins (0 and nbins+1 are under/overflow)
      results[ix - 1].push_back(htemp->GetBinContent(ix));
    }//ix
        
    //essential to delete the TChain, to stop a memory leak (which leads to the program slowing to a crawl)
    delete treco;
    //best delete htemp too
    delete htemp;
    
  }//end of loop over iiter


  //now we set the bin content & error of the histogram, using the information in the array of vector<float>
  for(int ix = 1; ix <= nbins; ix++) {
    float mean = TMath::Mean(results[ix - 1].begin(), results[ix - 1].end());
    float sd = TMath::StdDev(results[ix - 1].begin(), results[ix - 1].end());

    histo->SetBinContent(ix, mean);
    histo->SetBinError(ix, sd);
  }//ix

  //and finally draw the histogram
  TCanvas * c = new TCanvas();
  histo->Draw();
  
  //lets save it too
  c->SaveAs(TString::Format("%s.pdf", variable));

  return histo;
}
