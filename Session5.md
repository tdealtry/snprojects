# Session 5

## Text editors
- `gedit` is a good option for beginners used to `Ctrl+c` for copy, `Ctrl+s` for save, etc.
  - Open with `gedit &` or `gedit /path/to/filename.ext &`
  - The `&` is important! Without it, the terminal will be blocked until you close the `gedit` window
- Check `Session1.md` for more advanced options

## Logging into the cluster outside the computing room
In order to logon from outside, you need to connect to one of the following machines
- lapw.lancaster.ac.uk
- lapx.lancaster.ac.uk
- lapz.lancaster.ac.uk

For this, you should use the same username/password as you use in the computer room. This is **not** the same as you use for moodle, University email, etc. It's the password that Peter Love setup for you.

### Windows
[MobaXterm](https://mobaxterm.mobatek.net/) is a Windows application that will give you X11 (graphics!) access to the machines

You can logon by e.g.:
- `ssh tdealtry@lapx.lancaster.ac.uk` (I'm unsure if you need to type this out every time)
- Click `Session` on the bar, then `SSH`, then add in `lapx.lancaster.ac.uk` and your username, then click OK and it'll prompt you for your password. (I presume this gives a saved config you can reuse)

Note that the graphics will be slower than if you used an MPhys machine. And things will be dependent on your internet connection (When you're at Lancaster, things are faster. When you're in Japan in a meeting with 100+ other people, things are *sloowwwwww*)

### Linux
Try `ssh -X -Y tdealtry@lapx.lancaster.ac.uk`, and see if you can see graphics. If not, try without `-X` and without `-Y` separately, to see which one works

### MacOS
Same as for Linux, but you first need to install [XQuartz](https://www.xquartz.org/)

## Accessing TTree variables
So far, we've just used `TTree::Draw()` to get information from `TTree`s.
This is great for simple things, but you sometimes need to extract more information than you can do with `TTree::Draw()`.

`read_from_tree.C` is a macro that
- Creates an output file & tree to save the results for this SN
  - Different variables can be added for your use case
- Reads a single SN
- Sets up variables to read information from the tree (using `TTree::SetBranchAddress()`)
  - Different variables can be added for your use case
- Loops over every entry (reconstructed trigger) in that SN
  - Simply passes through the input Euler angles to the output
  - You can expand this to do more complex things (e.g. take dot products with [`Dot()`](https://root.cern.ch/doc/master/group__VectFunction.html#ga79153f06ebb205f0295f3064437ff47e))
- Saves the tree to the file

A more heavily commented `read_from_tree_extra_comments.C` is available, with 
```C++
/*
 more comments in multi-line comment blocks like these
*/
```

### Friend trees
With that file, you've created a `TTree` that is in sync with the input (i.e. the `n`th entry of your tree corresponds to the `n`th entry of the original tree).
That means you can add your tree as a _friend_ to the original tree, in order to be able to apply cuts using the original variables. But how?

```C++
.L chain.C+
TChain * t = chainit("/data/tdealtry/2021summerprod/sntools/v1.0.1/nakazato/NO_10kpc_2001/HyperK_20perCent/0000/wcsim/v1.9.4_C/triggerapp/v1.1.2_bonsaiv1.1.1_flowerv1.1_A/flbo/*.root");
//add the friend
chainafriend(t, "tree.root", "sn_analysis");
//check we can draw both old & new things
t->Draw("DirectionEuler[0]");
t->Draw("NewDirectionEuler[0]", "", "SAME");
```
They exactly overlap. Success!

This means you can now do things that use variables from both original & new `TTree`s, like
```C++
t->Draw("NewDirectionEuler[0]", "Energy>7", "SAME");
```

An alternative [ROOT TTree friend tutorial](https://root.cern.ch/doc/master/treefriend_8C.html)

## Looping over cut values
`loop_over_time.C` is an expansion of `spread.C`, which adds a new function (`loop_over_time()`)

Here you see an example of 
- how to loop over cut values (reconstructed time in this case)
- how to call another user-defined function in a macro file
- how you could use the output from that function

Note that it's not the most useful in its current form. But shows how you can do a loop & call another function of yours, if/when you need it. This can be useful for looping over cut values, looping over SN, or whatever else
