# Plot styling tips
[[_TOC_]]

The following is based on histograms (e.g. `TH1F`). If you're using graphs (e.g. `TGraph`) then things are mostly the same (except that graphs don't have a `TAxis` until after you draw them)

If you want to follow along (to understand before adapting for your code) here's a test setup to do in a ROOT session
```C++
//create a canvas
TCanvas * c = new TCanvas();
//create a legend, in a bad location
TLegend * legend = new TLegend(0.9, 0.9, 1.0, 1.0);
//create a histogram
TH1F * h = new TH1F("hname",";X title;Y title", 10, 0, 100);
//fill it with random numbers (uniformly)
TRandom3 random;
for(int inum = 0; inum < 1000; inum++) {
	h->Fill(random.Uniform(0, 100));
}
h->Draw();
```
You may need to do something like manually resize the canvas for the changes to occur (`c->Update()` should theoretically work, but doesn't for me)

## Styles for lines/markers/fills
Check out these pages for what all the styles/colours/sizes/etc. are available
- [Colours](https://root.cern.ch/doc/master/classTColor.html#C02)
- [Line](https://root.cern.ch/doc/master/classTAttLine.html)
- [Marker](https://root.cern.ch/doc/master/classTAttMarker.html)
- [Fill](https://root.cern.ch/doc/master/classTAttFill.html)

Then you'll want to do things like `h->SetMarkerStyle(20);`

A reminder that as well as styles & colours, you can also change marker size (`h->SetMarkerSize(1.5)`) and line width (`h->SetLineWidth(2)`)

## Fine tuning
There are two ways to do things
- Use `Get...()` to get the current position/text size/etc. e.g. `h->GetXaxis()->GetTitleOffset()`. This is generally the method I use, but certainly sub-optimal for fine tuning!
- Keep a canvas open that has one version of the plot. Drag things around, right click on things to set text size, etc. Then, when you're happy, you can right click on a feature and do `Dump` to get all the relevant variable values e.g. dumping a `TAxis` you get a massive list including info about the label text position/size
```
fLabelOffset                  0.005               Offset of labels
fLabelSize                    0.04                Size of labels
```

Of course the idea in both cases is if you have `N` plots of the same type, you fine tune & test it on `1` plot only, then when you're happy run on all plots (then inevitably realise one plot is slightly hidden by the legend, because it's a slightly different shape...)

## Canvas margins
In order to remove white space around your plot, you can use this to 
* For top/right, this should work in all instances (you may want to trim it even further (use a smaller number than `0.05`))
  * When drawing the z-axis with e.g. `"COLZ"`, you'll need a larger margin
* For left/bottom, you may need to fiddle by hand (so you don't cut off your axis title) - so I commented it out in the example
```C++
c->SetTopMargin(0.05);
c->SetRightMargin(0.05);
//c->SetBottomMargin(0.18);
//c->SetLeftMargin(0.18);
```

## Legends
If you're only plotting a line, you don't need to include the marker style in the legend.
You achieve this by
```
legend->AddEntry(h, "line only", "L");
legend->AddEntry(h, "line + vertical error bar", "LE");
legend->AddEntry(h, "marker only", "P");
legend->AddEntry(h, "fill only", "F");
legend->AddEntry(h, "marker & line", "PL");
legend->Draw();
```
Basically you do combinations of `P`, `L`, `LE`, `F` as required

## Stats box
If you don't need it for a plot, turn it off with
```C++
gStyle->SetOptStat(0);
```

## Axis titles
If they're off the plot (happens if the axis labels are too *long*), you've two options
- If there's space, move the axis title closer to the axis labels (see here)
- If there's not, alter the canvas margin (see [above](https://gitlab.com/tdealtry/snprojects/-/blob/master/PlotStyle.md?ref_type=heads#canvas-margins))

In order to do this, you'll probably first want to check what the default is, then use that to inform your choice
```C++
cout << "X axis offset: " << h->GetXaxis()->GetTitleOffset() << endl;
h->GetXaxis()->SetTitleOffset(0.15);
```

## Logging axes
Whether to draw axes logged (base `e`) is a canvas property... which is done for the horizontal axis as:
```C++
c->SetLogx();
```

And similarly for the vertical axis as:
```C++
c->SetLogy();
```

Note that it is much easier to do things this way (calculating `log(value)`), as for the other way
- you need to recalculate errors (and it may be tricky to do this correctly)
- the axis numbers are less clear
- the logging the horizontal axis, you'd need to do some kind of variable binning (I guess)

## Text size
Be careful to ensure that any text is clear and legible. In general, there are 3 places plots can appear
- In a report/paper
- On a poster
- In a presentation

A rough rule of thumb is that the minimum text size on a plot (e.g. axis labels) should be similar to the report text size.
(Note that this is personal opinion. Some might say that the axis title size should be similar to the report text size).

For presentations, it depends on your presnetation style. Do you have 1 plot per page? Or lots?
The more plots, the smaller the plot size, so the larger the text size on the plot has to be.

For axes, we have (do modify the numbers for you use case!)
```C++
h->GetXaxis()->SetLabelSize(0.04);
h->GetXaxis()->SetTitleSize(0.04);
```

For legends, it's a mess. Basically you have no clear control, and you have to set the legend text size via the legend size
```C++
//make a copy of the original legend
TLegend * legend2 = new TLegend(*legend);
//move the co-ordinates of the new legend
legend2->SetX1NDC(0.5);
legend2->SetY1NDC(0.5);
legend2->SetX2NDC(0.8);
legend2->SetY2NDC(0.8);
//draw the new legend
legend2->Draw();
```

## Plot titles
For plots put into the report, you'll have a caption. This means you don't need a plot title
```C++
h->SetTitle("");
```
or alternatively, if you want to set the axis titles at the same time
```C++
h->SetTitle(";X title;Y title;Z title");
```
`";Z title"` can be omitted if you have no z-axis

For plots in a talk, my personal opinion is that you still don't need a plot title e.g.
- x-axis titled "X"
- y-axis titled "Y"
- Then a plot title of "Y vs X" is redundant, since that information is already in the axis titles

But **please** do check with Helen, as she knows the mark scheme (I don't!)

## Axis range
Most of the time, ROOT does a decent job of setting the range you see when you draw. e.g. for a 1D histogram
- By default, all x-axis bins are drawn
- By default, the y-axis goes from 
  - 0 to (maximum * 1.05) if the min & max are positive, and min is close to 0
  - (minimum * 0.95) to (maximum * 1.05) if the min & max are positive, and min is further away from 0
  - Similar things happen for negative numbers...

But sometimes you need to set the range yourself to highlight a region, for asthetics, or to show missing data when using the `"SAME"` drawing option

You do this with e.g.
```C++
h->GetYaxis()->SetRangerUser(0, 100);
```

Rather than setting by hand, you can do this automatically e.g.
```C++
h->GetYaxis()->SetRangeUser(0, 1.05 * h->GetMaximum());
```

This is especially useful if you have histograms `h` and `h2` that you're drawing on the same axes
```C++
h->GetYaxis()->SetRangeUser(0, 1.05 * TMath::Max(h->GetMaximum(), h2->GetMaximum()));
```
Note that this is required to be done only for the first histogram you draw (i.e. the one drawn without `"SAME"`)


For the x-axis, this is similar, but is constrained to show all of a bin (so if you have bin edges at 0, 1, 2, ... you can’t draw showing from 0.5 and above, it’d have to be from 0 and above)

## Canvas ticks
By default, ticks are put on the bottom & left axes, but not on the top & right axes.
It can be useful to add ticks to these using `TCanavs` methods.

```C++
c->SetTickx();
c->SetTicky();
```

Personally, I think it looks nicer to include ticks around all 4 edges.

## Canvas grid
Occasionally useful, you can add grid lines, to one or both axes

```C++
c->SetGridx();
c->SetGridy();
```
