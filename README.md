# New starter notes for Hyper-K Supernova projects
[[_TOC_]]

The idea of this repository is to show with, worked examples, how to analyse Hyper-K Supernova Monte Carlo files.

For a more detailed reference on various aspects, see the "Lancaster Particle Physics Linux Computing Notes: Hyper-K edition". But be warned that this has bloated over the past few years.

Note that you can
- Read online.
  - By default, gitlab uses a dark-on-light background. If you create an account and login, you can alter your preferences to select a light-on-dark option, and also alternative syntax highlighting colours.
  - You can increase font size by using the web browser zoom options (e.g. `<Ctrl>++`)
- Download and open in another file. Typically you will have more control viewing content in this way
  - For C++ code, I recommend downloading to the text editor you use for coding, as you can set the view options there (colours, font, text size, etc.).
  - For markdown (the `.md` files like this, and `Session1.md` etc.) you can download & open it in a browser with an extension installed, or a separate piece of software (to start with, do check your text editor, but I have a feeling that it will not render tables correctly). Please do your own research for what software is best for your use case, as, for example, I have not found a popular browser extension that is security verified by Mozilla (Firefox owners).

## Index
The idea of this section is to provide a little summary of what information is where. Warning: this list is likely incomplete

### Linux cluster basics
- [Session1:Linux terminal](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session1.md?ref_type=heads#linux-terminal) for a quick run through of some terminal basics (`ls`, `cd`, text editors, etc.)
- [Session5:Text editors](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session5.md?ref_type=heads#text-editors) for more on text editors
- [Session5:Logging into the cluster outside the computing room](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session5.md?ref_type=heads#logging-into-the-cluster-outside-the-computing-room) for how to logon to the Lancaster particle physics cluster from your laptop (e.g. use MobaXterm with Windows)

### ROOT basics
- [Session1:ROOT](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session1.md?ref_type=heads#root) for a quick run through of some ROOT (how to open a `TBrowser`, how do a basic `TTree::Draw()`, how to exit ROOT, etc.)
- [Session2:More on plotting a `TTree` with ROOT](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session2.md?ref_type=heads#more-on-plotting-a-ttree-with-root) for some more advanced `TTree::Draw()` features (using `"COLZ"` for 2D plots, applying cuts, using `"SAME"` to draw multiple histograms on the same axes)
  - [Session4:Applying multiple cuts to `TTree::Draw()`](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session4.md?ref_type=heads#applying-multiple-cuts-to-ttreedraw) shows how to apply multiple cuts at once
- [Session2:ROOT analysis macros](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session2.md?ref_type=heads#root-analysis-macros) for how to write, compile, and run a ROOT macro. The example macro allows you to `TChain` all files from a single SN
  - [Session3:ROOT macro arguments](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session3.md?ref_type=heads#root-macro-arguments) for how to compile & run macro functions that have arguments
  - [Extras:Adding only some files to a `TChain`](https://gitlab.com/tdealtry/snprojects/-/blob/master/Extras.md?ref_type=heads#adding-only-some-files-to-a-tchain) for how to add only files in a certain time range to the `TChain`
- [Session3:Plotting more complicated things on an axis](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session3.md?ref_type=heads#plotting-more-complicated-things-on-an-axis) shows how to plot combinations of variables with `TTree::Draw()` (e.g. reconstructed `r`, reconstructed direction `x,y,z`)
- [Session3:Saving plots](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session3.md?ref_type=heads#saving-plots) for how to save a `TCanvas` to a file (remember that `.pdf` is better than `.png`!)
  - `spread.C` gives an example of how to save a drawn histogram to a `.pdf`
  - `loop_over_time_savepdf.C` gives an example of how to save multiple plots in a single `.pdf`. See also [Session6:Saving multiple histograms to a pdf file](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session6.md?ref_type=heads#saving-multiple-histograms-to-a-pdf-file)
- [Session3:Getting the results of a `TTree::Draw()`](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session3.md?ref_type=heads#getting-the-results-from-a-ttreedraw) shows how to extract the drawn histogram (named `"htemp"` by default) from the `TTree::Draw()` command. This allows you to do statistical things with the histogram (e.g. get the mean) and aesthetic things (e.g. change the line color)
  - `spread_plot.C` gives an example of setting different aesthetics for multiple lines in a `for` loop
- [Session3:Comparing many histograms I](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session3.md?ref_type=heads#comparing-many-histograms-i-plotting-many-of-the-same-histogram) shows how to use `TString::Format()` to form strings from variables (e.g. inserting an integer into a string)
  - This is a useful technique in multiple situations. It is used in almost all the macros in this repository, in order to form filenames (both for input and output)
- [Session3:Comparing many histograms III](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session3.md?ref_type=heads#comparing-many-histograms-iii-plotting-summary-information-from-different-sn) shows how to create, fill, and write a `TTree`
  - See [Session6:Nuances of `Branch()` and `SetBranchAddress()`](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session7.md?ref_type=heads#nuances-of-branch-and-setbranchaddress) for details of how to use the pointer argument for associating variables with a `TTree` when reading or writing
- [Session4:Saving histograms to file](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session4.md?ref_type=heads#saving-histograms-to-file) shows how to save histograms to an output ROOT file, using `spread_and_save.C`
- [Session4:Reading histograms from file](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session4.md?ref_type=heads#saving-histograms-to-file) shows how to read in (and plot) histograms from an input ROOT file, using `spread_plot.C`
- [Session4:Plot legends](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session4.md?ref_type=heads#plot-legends) shows how to add a `TLegend` to your plot using `spread_plot_legend.C`. This is essential when you have more than 1 line on your plot
- [Session4:Turning the stats box off](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session4.md?ref_type=heads#turning-the-stats-box-off) shows how to turn the statistics box off (that shows histogram name, mean, standard deviation, etc.)
- [Session4:Histogram errors](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session4.md?ref_type=heads#histogram-errors) provides a code snippet you can use to set histogram bin errors based on `sqrt(bin content)`. Using `sqrt(bin content)` is a useful, well-defined error in some circumstances, but not in others (e.g. `spread()` uses the standard deviation) - so you need to be careful when using the function! You can use the function where relevant, and make other versions as required
  - [Wikipedia's propagation of uncertainty formulae](https://en.wikipedia.org/wiki/Propagation_of_uncertainty#Example_formulae) is a useful resource/reminder for what the uncertainty on a measurement is
- [Session6:Adding text to a plot](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session6.md?ref_type=heads#adding-text-to-a-plot) describes how to use a `TLatex` to add extra text to a plot
- [Session7:Understanding histogram binning](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session7.md?ref_type=heads#understanding-histogram-binning) describes how to get histogram bin information using `GetBinCenter()`, `GetBinLowEdge()`, and `GetBinUpEdge()`
  - [Extras:Understanding histogram binning](https://gitlab.com/tdealtry/snprojects/-/blob/master/Extras.md?ref_type=heads#variable-histogram-binning) for how to setup histograms with non-uniform binning
- [Session7:Histogram integrals](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session7.md?ref_type=heads#histogram-integrals) describes how to integrate histograms (both the entire histogram, and ranges)
- [Session7:Histogram projections](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session7.md?ref_type=heads#histogram-projections) describes how to take 1D projections of a 2D histogram

### Supernova files
- [Session1:The types of files](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session1.md?ref_type=heads#the-types-of-files) describes the variables in the true sntools (`sntk`) files, and the reconstructed TriggerApp (`flbo`) files
- [Session1:The types of Supernova](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session1.md?ref_type=heads#the-types-of-supernova) describes the SN we have, including what the different file path labels mean
  
### Comparing many histograms
There are many ways to do this. You need to choose the method that best meets the requirements for each plot you're creating
1. [Session3:Plotting many versions of the same histogram (for the same SN configuration)](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session3.md?ref_type=heads#comparing-many-histograms-i-plotting-many-of-the-same-histogram) has a code snippet that uses the `"SAME"` feature to draw many copies of the same plot (different statistical repeats of the same SN progenitor)
2. [Session3:Plotting the spread from many histograms (can compare all SN for a given model, and compare a handful of SN configurations)](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session3.md?ref_type=heads#comparing-many-histograms-ii-plotting-the-spread-from-many-histograms) talks about the `spread.C` macro, that, for a single progenitor (chosen by the macro function arguments), loops over each statistical repeat to create a histogram with containing the mean of each bin (with errors as the standard deviation)
   * `spread_and_save.C` is a minor extension, that writes the `TH1F` histogram to an output ROOT file (instead of saving a `.pdf` of the drawn histogram)
3. [Session3:Plotting summary information from different SN (e.g. mean reconstructed energy) (can compare multiple SN configurations)](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session3.md?ref_type=heads#comparing-many-histograms-iii-plotting-summary-information-from-different-sn) talks about the `analysis.C` macro, that, for each SN, creates an output `TTree` that contains summary information for that SN (e.g. mean reconstructed energy & mean true energy)

### Running over many SN progenitors
- `spread.C` (and its descendants) shows how to loop over all statistical repeats of a chosen progenitor inside the *worker* macro function
  - You can run over multiple progenitors by calling the *worker* macro function multiple times (e.g. in `for` loops)
	- e.g. if your *worker* macro function is called `spread()`. You can create a `spread_run_all()` function that calls . Doing it this way allows you to test `spread()` by just calling `spread()` (which should be fairly quick!). Then when you're ready to check all SN, you can run `spread_run_all()`
- `analysis.C` shows how to loop over all SN progenitors and statistical repeats inside the *worker* macro function
  - There is a `max_sn_to_analyse` argument which allows you to test the macro on low statistics, before running over all SN when you're ready

### Running with many different cut values
- [Session5:Looping over cut values](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session5.md?ref_type=heads#accessing-ttree-variables) describes `loop_over_time.C` (an expansion of `spread.C`), which shows how to step through cut values (time in this case) to acquire the same histogram multiple times with different values of the cut. This example just prints out the mean of the distribution, but you could modify it to draw the histogram

### Friend trees
Sometimes you need to do complicated things with variables (too complicated for a `TTree::Draw()`), meaning you need to access the variables inside the `TTree` themselves (rather than the histograms they create).

Writing a *friend* tree allows you to create your complex variable on an event by event (= trigger by trigger = entry by entry) basis, such that you can then use `TTree::Draw()` commands as usual, along with any cuts you require
#### Writing a friend `TTree`
- [Session5:Accessing `TTree` variables](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session5.md?ref_type=heads#accessing-ttree-variables) describes the macro `read_from_tree.C` that describes how to 
  - Note that a more heavily commented version `read_from_tree_extra_comments.C` is available
#### Reading a friend `TTree`
- [Session5:Friend trees](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session5.md?ref_type=heads#accessing-ttree-variables) shows how to associate your newly written `TTree` as a friend of the original `TTree`, such that you can then `TTree::Draw()` using variables from both new & old `TTree`s

### Statistical tests
[Session6:Statistical tests](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session6.md?ref_type=heads#statistical-tests) describes some statistical testsROOT has that allow you to compare two distributions. There is also a discussion about how you might define your own test, if required (e.g. if you need to use a test that ROOT doesn't have).
- See `ks_test_unbinned.C` for how to perform tests with unbinned data
- See `ks_test_binned.C` for how to perform tests with binned data (e.g. histograms)

### `TGraph`
[Session7:`TGraph`s](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session7.md?ref_type=heads#tgraphs) introduces `TGraph`s, an alternative plotting class that is useful when you have irregular binning (e.g. plotting a quantity vs simulated SN distance)
- Use `TGraph` when you have no errors
- Use [`TGraphErrors`](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session7.md?ref_type=heads#symmetric-errors) when you have symmetric errors
- Use [`TGraphAsymmErrors`](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session7.md?ref_type=heads#asymetric-errors) when you have asymmetric errors

### `TVector3`
[Session7:`TVector3` maths](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session7.md?ref_type=heads#tvector3-maths) introduces `TVector3`s, a 3-vector class with lots of in-built mathematical operations e.g.
- [Dot product](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session7.md?ref_type=heads#dot-product)
- [Addition](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session7.md?ref_type=heads#addition)

### Fitting a function to a histogram
[Session8:Fitting a function to a histogram/graph](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session8.md?ref_type=heads#fitting-a-function-to-a-histogramgraph)

### Stacking histograms
[Session8:Stacking histograms](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session8.md?ref_type=heads#stacking-histograms)

### Sorting numbers
[Session8:Sorting numbers](https://gitlab.com/tdealtry/snprojects/-/blob/master/Extras.md?ref_type=heads#sorting-numbers)

### Plot styling tips
[Plot styling tips](https://gitlab.com/tdealtry/snprojects/-/blob/master/PlotStyle.md?ref_type=heads) has information about how to improve the look of your plots
