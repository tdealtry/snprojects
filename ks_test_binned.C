#include "chain.C"
#include "TH1F.h"
#include "TPad.h"

//Define a global variable, such that its value persists
// after get_histo() has incremented it
int ihist = 0;

/*
  Define a function called get_hsoto
  - TH1F * means that the function returns a pointer to a 1D histogram
  - The argument is the file path of the TChain you want to load
*/
TH1F * get_histo(const char * input_file_path)
{
  //Open the reconstructed TChain, as usual
  TChain * t = chainit(input_file_path);
  //Draw the distribution of interest (Reconstructed Time)
  // - You can apply cuts in the usual TTree::Draw() way, e.g. to add the standard fiducial volume & minimum energy cuts
  t->Draw("Time");
  //Get the histogram we've just drawn
  auto htemp = (TH1F*)gPad->GetPrimitive("htemp"); // 1D
  //make a copy of htemp, such that this htemp doesn't disappear the next time we call get_histo()
  // (really that it doesn't disappear the next time TTree::Draw() is called)
  TH1F * h = new TH1F(*htemp);
  //give it a new name too, to prevent it being overwritten
  // This will be h0, h1, h2, ...
  h->SetName(TString::Format("h%d", ihist));
  ihist++;
  //return the copy of the histogram
  return h;
}

/*
  Define a function called ks_test_binned
  - double means that the function returns a number (the result of the KS test)
*/
double ks_test_binned()
{
  //Get the histograms of interest
  TH1F * h0 = get_histo("/data/tdealtry/2021summerprod/sntools/v1.0.1/nakazato/NO_10kpc_2001/HyperK_20perCent/0000/wcsim/v1.9.4_C/triggerapp/v1.1.2_bonsaiv1.1.1_flowerv1.1_A/flbo/*.root");
  TH1F * h1 = get_histo("/data/tdealtry/2021summerprod/sntools/v1.0.1/nakazato/NO_100kpc_2001/HyperK_20perCent/0001/wcsim/v1.9.4_C/triggerapp/v1.1.2_bonsaiv1.1.1_flowerv1.1_A/flbo/*.root");

  //Perform the KS test
  double result = h0->KolmogorovTest(h1, "D");

  //Closure test: check what happens when the 2 distributions are identical (the same SN & same histogram)
  h0->KolmogorovTest(h0, "D");

  return result;
}
