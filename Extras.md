# Extras
[[_TOC_]]

## Adding only some files to a `TChain`
The `chainit()` function we've used before uses wildcards (`*`) to grab all files in a particular directory. This can be slow when using 20 second duration Nakazato model files, especially since in most cases a 1.5 second cut will be applied.

So there's now an extra function `chainit_range()` in `chain.C` that only opens the files in the range you give it

```C++
.L chain.C+

//Use the original chainit() for comparison
TChain * tall = chainit("/data/tdealtry/2021summerprod/sntools/v0.7.2/nakazato/NO_10kpc_2001/HyperK_20perCent/0000/wcsim/v1.9.4_A/triggerapp/v1.1.1_bonsaiv1.1.1_flowerv1.1_A/flbo/*flbo.root");

//Use the new function
const char * file_path = "/data/tdealtry/2021summerprod/sntools/v0.7.2/nakazato/NO_10kpc_2001/HyperK_20perCent/0000/wcsim/v1.9.4_A/triggerapp/v1.1.1_bonsaiv1.1.1_flowerv1.1_A/flbo/sn_0000_NUMBER_flbo.root";
const int first_file_number = 0;
const int last_file_number = 2000;
int file_convention = 0;
TChain * tsome = chainit_range(file_path, file_convention, first_file_number, last_file_number);

//And use it on a newer set of files, that have a different filenaming convention (no trailing 0s)
const char * file_path2 = "/data/tdealtry/2021summerprod/sntools/v1.0.1/nakazato/NO_10kpc_2001/HyperK_20perCent/0000/wcsim/v1.9.4_D/triggerapp/v1.1.2_bonsaiv1.1.1_flowerv1.1_A/flbo/sn_0000-NUMBER_flbo.root";
file_convention = 1;
TChain * tsome = chainit_range(file_path2, file_convention, first_file_number, last_file_number);
```
Notes
- The `file_path` should have no wildcards
- The `file_path` should contain `NUMBER` in the relevant place, where the 9 digit file number is in the path. What happens is that `NUMBER` is substituted for every number in the range given by the `_file_number` arguments (in a loop)
- The `_file_number` arguments are inclusive (in the above example files 0 to 2000 inclusive are included in the `TChain`)
- You should work out which `_file_number` range you need for your studies
  - In order to do this, you should check the minimum & maximum times for each batch of SN (where *batch* means one of the four sets of Nakazato/Warren 1.5/20 second & PMT dark rates listed [here](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session1.md?ref_type=heads#summary)).
  With this information you will be able to i) set a consistent high & low time cuts, so that SN batch comparisons are valid and ii) use the time cut information to determine which `file_number`s are required for use in `chainit_range()`
  
  - It is non-trivial to create a `TChain` with all files in (multiple wildcards `*` are not allowed with `TChain::Add()`) so I setup a function to do this.
  ```C++
  .L chain.C+
  //Get Nakazato 20s 5kHz
  TChain * t = get_sn_batch_chain(0);
  
  // use different numbers as the argument to get the other batches (valid range is 0-3)
  ```
- `file_convention` is important, as the file naming convention changed between Nakazato 20s & Nakazato 1.5s
  - For Nakazato 20s use `file_convention = 0` (this has a trailing zero, so the 9 digit number is in fact an 8 digit number followed by a 0)
  - For Nakazato 1.5s use `file_convention = 1` (this has no trailing zero, so the 9 digit number is a true 9 digit number)
  - For Warren 1.5s, test it out & see which is valid

## Sorting numbers
Say you have some numbers you'd like to sort. How to do that?
1. Put your numbers into a [`std::vector`](https://cplusplus.com/reference/vector/vector/vector/)
2. Sort the `vector` into ascending order using [`std::sort()`](https://cplusplus.com/reference/algorithm/sort/)
3. If required, change it to descending order using [`std::reverse()`](https://cplusplus.com/reference/algorithm/reverse/)

## Variable histogram binning
So far, we've made histograms with constant binning e.g. to make a 10-bin 1D histogram in the range 0 to 1, we've done this
```C++
TH1D * h = new TH1D("h", "Title", 10, 0, 1);
```
which gives a histogram in which all bins have the same width (0.1) with edges: `[0,0.1]`, `[0.1,0.2]`, ..., `[0.9,1.0]`.

It can be useful to create a histogram with variable binning (e.g. to suppress empty bins).
In order to do this, you need to setup the bin edges yourself.
```C++
vector<double> edges = {0.0, 0.1, 0.2, 0.3, 0.5, 0.75, 1.0};
const int nbins = edges.size() - 1;
TH1D * hvar = new TH1D("hvar", "Variable binned histogram title", nbins, edges.data());
```

Unfortunately, you can't replace a `TTree::Draw()` command like this directly in a single line
```C++
t->Draw("Energy>>h(10,0,1));
```
Instead you have to do this
```C++
vector<double> edges = {0.0, 0.1, 0.2, 0.3, 0.5, 0.75, 1.0};
const int nbins = edges.size() - 1;
TH1D * hvar = new TH1D("hvar", "Variable binned histogram title", nbins, edges.data());
t->Draw("Energy>>hvar");
```
