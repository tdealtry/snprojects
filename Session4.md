# Session 4
[[_TOC_]]

## Last time
Last time we learnt how to
- Plot more complicated variables e.g. reconstructed `r` position with `t->Draw("sqrt(Vertex[0]*Vertex[0] + Vertex[1]*Vertex[1])");`
- Write & run ROOT macros
- Saving plots as `pdf`s e.g. with `c->SaveAs("Energy.pdf");`
- Getting the results of `TTree::Draw()` e.g. with `auto htemp = (TH1F*)gPad->GetPrimitive("htemp"); // 1D`, so that you can then modified the aesthetics (e.g. titles, line colors, ...) but also grab statistics from the histogram (e.g. `double mean = htemp->GetMean();`)
- How to compare histograms
  1. Plotting many versions of the same histogram (for the same SN configuration)
  2. Plotting the spread from many histograms (can compare all SN for a given model, and compare a handful of SN configurations)
  3. Plotting summary information from different SN (e.g. mean reconstructed energy) (can compare multiple SN configurations)

## Saving histograms to file
The ROOT macro `spread.C` from last time calculates the spread of a single SN configuration, then draws the resulting histogram, before saving the histogram to PDF.

In order to compare the spread of different SN configurations, there are a couple of methods
1. Modify the macro to save the histogram
2. Modify the macro so that it doesn't create the `TCanvas` within it. And also pass a way to use `"SAME"` or not when drawing a histogram

I strongly recommend 1., primarily because whilst `spread.C` is currently fast, you may want to modify it to do complex (slow) operations. In this instance it is much better (quicker) to create each histogram once, then (after checking it looks ok) having another macro to draw it as you'd like

I've therefore created `spread_and_save.C`, which is an extension of `spread.C`. You can see what's different by doing this in the normal (non-ROOT) terminal
```bash
diff spread.C spread_and_save.C
```

You can run, for example with
```
root -b -q spread_and_save.C+
```
You should see a file called `spread_Energy_nakazato_NO_10_2001.root`, that contains a histogram called `Energy`

In order to use it in a macro, you'll want to do something like
```C++
TFile f("spread_Energy_nakazato_NO_10_2001.root");
TH1F * h = 0;
f.GetObject("Energy", h);
```
you can then e.g. `h->Draw()`

Then, if you have multiple configurations you want to compare e.g. check out the `spread_plot.C` macro

## Plot legends
When you have multiple lines on a plot, you **have** to label each one (not doing so is in the same category as not having axis labels!).
You do this with a legend

I've written an extension of `spread_plot.C` called `spread_plot_legend.C`, and you can see the extra bits I've added with
```bash
diff spread_plot.C spread_plot_legend.C
```

Run `spread_plot_legend.C` & see what you get

A general note that placing legends nicely is non-trivial. It can be useful to make the plot, then drag the legend to somewhere nice, before saving.
If you want to know where you've moved it to (for setting in your macro)
* Right click on the legend on the canvas
* Ensure you've clicked in the correct place (**TLegend::TPave** should be the header)
* Click `Dump`
* Find the lines (nearish the top) like
  ```
  fX1NDC                        0.591691            X1 point in NDC coordinates
  fY1NDC                        0.495798            Y1 point in NDC coordinates
  fX2NDC                        0.89255             X2 point in NDC coordinates
  fY2NDC                        0.697479            Y2 point in NDC coordinates
  ```
  here `NDC` refers to "canvas space" (number is fraction of the canvas). Whereas the later `fX1` etc. refers to "axis space" (number is based on the axis range)

## Turning the stats box off
All you plots so far have a stats box, which has for example the number of entries & mean of a single histogram.
When comparing multiple plots, this is less relevant

The line to turn the box off is
```C++
gStyle->SetOptStat(0);
```
If you're doing this in a macro, the relevant header is
```C++
#include "TStyle.h"
```

Note that there is a way to draw multiple stats boxes for multiple histograms (and ways to e.g. change the text color such that it's obvious which box refers to which histogram). If you need this functionality at any point, please let me know

## Histogram errors
Whilst ROOT can automate error setting for you, exactly what it does is confusing. So it's often easier to calculate the error yourself.

I therefore recommend this little function, which calculates the statistical error on a bin (where the bin is a *simple* variable, where *simple* means it's a single variable, and has no operations applied to it (`Log(Energy)` and reconstructed `r` position are examples of **not** simple variables))
```C++
void ApplySimpleStatError1D(TH1F * h)
{
	//loop over every bin (ignoring underflow & overflow)
	for(int ix = 1; ix <= h->GetNbinsX(); ix++) {
		//Get the current bin content
		float content = h->GetBinContent(ix);
		//Get the bin error. In this simple case, it is simply sqrt(content)
		float error = sqrt(content);
		//Inform the histogram what the current bin error is
		h->SetBinContent(ix, error);
	}//ix
}
```

I recommend creating a file that has this function defined inside, that you can `#include` in the multiple macros you'll want to use it with (in the same way we have the `chainit()` function inside `chain.C`, and `#include "chain.C"` in multiple macros)

This function should be called before drawing it. e.g.
```C++
t->Draw("Energy");
auto htemp = (TH1F*)gPad->GetPrimitive("htemp"); // 1D
htemp->SetLineColor(kRed);
ApplySimpleStatError1D(htemp);
htemp->Draw();
```

## Applying multiple cuts to `TTree::Draw()`
In order to apply multiple cuts, you can chain cuts together with the logical AND operator `&&`, which requires all things to be true in order to be included in the drawn histogram

For example, to draw the reconstructed energy between 10 & 50 MeV (inclusive)
```C++
t->Draw("Energy", "Energy<=10&&Energy>=50");
```
You can chain many such operations together, such that you can do e.g. a minimum energy cut combined with a fiducial volume cut (in both `r` and `z`)

Note that the logical OR operator `||` may also be useful in setting your cuts. For example, to draw the reconstructed energy outside the range 10-50 MeV
```C++
t->Draw("Energy", "Energy<10||Energy>50");
```
