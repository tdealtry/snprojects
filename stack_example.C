#include "chain.C"
#include "TROOT.h"
#include "TChain.h"
#include "THStack.h"
#include "TH1F.h"
#include "TPad.h"
#include "TString.h"

void stack_example()
{
  // Read in a truth sntools file.
  TChain * t = chainit("/data/tdealtry/2021summerprod/sntools/v1.0.1/nakazato/NO_10kpc_2001/HyperK_20perCent/0000/sntk/*.root", "sntools");

  // Create a THStack that we can add histograms to
  THStack * stack = new THStack("stack", "True neutrino energy, broken down by neutrino flavour");

  // Define the list of possible neutrino flavours
  std::vector<int> neutrino_flavours = {-12, +12, -14, +14, -16, +16};
  // The following should be the same order as we want to use above
  // Histogram titles (used by the legend)
  std::vector<string> neutrino_flavours_str = {"#bar{#nu}_{e}", "#nu_{e}", "#bar{#nu}_{#mu}", "#nu_{#mu}", "#bar{#nu}_{#tau}", "#nu_{#tau}"};
  // And some styles
  std::vector<Color_t> colors = {kBlack, kRed, kBlue, kGreen+2, kYellow-1, kGray};
  std::vector<Color_t> fills = {1001, 3005, 3395, 3144, 3003, 3019};
  
  // Loop over the neutrino flavours
  for(unsigned int iflav = 0; iflav < neutrino_flavours.size(); iflav++) {
    // Check whether there are any events in the histogram from this neutrino flavour
    int n = t->GetEntries(TString::Format("nu_pdg==%d", neutrino_flavours[iflav]));
    // If not, continue
    if(n == 0)
      continue;

    // Draw the histogram, for the given flavour
    //  Note that you MUST give the binning you want to use to Draw()
    //  Why? If not, each histogram will have different binning, meaning they cannot
    //  be correctly stacked
    t->Draw("nu_totalenergy>>htemp(20, 0, 100)", TString::Format("nu_pdg==%d", neutrino_flavours[iflav]));

    // Get the histogram
    auto htemp = (TH1F*)gPad->GetPrimitive("htemp"); // 1D

    // Copy the histogram
    TH1F * hcopy = new TH1F(*htemp);

    // Give the copy a unique name
    //  (be wary about using special characters like "=" in a name)
    hcopy->SetName(TString::Format("nu_pdg%d", neutrino_flavours[iflav]));
    
    // Set the title, for the legend
    hcopy->SetTitle(neutrino_flavours_str[iflav].c_str());

    // Give the histogram a unique style
    hcopy->SetFillColor(colors[iflav]);
    hcopy->SetFillStyle(fills[iflav]);

    // Add the copy to the stack
    stack->Add(hcopy);
  }//iflav

  // Draw the stack
  stack->Draw();

  // Now we can set the stack x & y axis labels
  stack->GetXaxis()->SetTitle("True neutrino energy (MeV)");
  stack->GetYaxis()->SetTitle("Number of events in bin");

  // Create and draw an automatic legend
  //  The arguments are
  //   - position (xmin, ymin, xmax, ymax), in fraction of canvas co-ordinates
  //   - legend title
  //  If this doesn't work, check "Session4: Plot legends" to build a legend by hand
  gPad->BuildLegend(0.75,0.75,0.95,0.95,"");
}
