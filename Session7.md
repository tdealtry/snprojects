# Session 7
[[_TOC_]]

Since you've started making your own macros, I think it's more beneficial now to provide snippets that you can pick the relevant bits out from, (rather than infinite variations of `spread.C`!!!)

## Nuances of `Branch()` and `SetBranchAddress()`

The second argument of `Branch()` and `SetBranchAddress()` requires a **pointer**.

Why? Whenever we e.g. read an entry from a tree (`t->GetEntry()`), the way that the `TTree` is setup is that the pointer points directly to the contents of the file. The other way would be to copy all the data into your variables, which is just inefficient (slow and a waste of computer memory) and breaks for massive files.

How to do this in practice?
- If you have a single number, you need to use an ampersand (&) to ask for the pointer to the variable
  ```C++
  int number;
  output->Branch("number", &number, "number/I");
  ```
- If you have an array of numbers, you don't need to use an ampersand (&) to ask for the pointer, because the variable name is actually a pointer (e.g. `numbers` is a pointer, whereas `numbers[0]` is the value of the first entry)
  ```C++
  float numbers[10];
  output->Branch("numbers", numbers, "numbers/F");
  ```
- If you have an instance of a class (e.g. a `std::string`), you need to use an ampersand
  ```C++
  std::string string;
  output->Branch("string", &string);
  ```

The examples above are for `Branch()` (when writing a tree), but the same rules should be followed when using `SetBranchAddress()` (when reading a tree)

## Understanding histogram binning
Most of the example we've been through we've let ROOT determine the binning automatically based on the lowest & highest values of the variable(s) being plotted, and some algorithms ROOT uses to determine how many bins to use.

**Note** whilst this is often fine, it breaks down in some circumstances (e.g. if there is an outlier).
See `spread.C` and its relations for how to tell `TTree::Draw()` what binning you would like
```C++
treco->Draw(TString::Format("%s>>myhtemp(%d,%d,%d)", variable, nbins, xmin, xmax)
```

It can be useful to programatically get the binning information (whether you've designed the binning yourself, or not).

I have a little macro I use fairly often to check binning.
If required, you can use the concepts & methods inside to grab the binning for your use case (e.g. creating a `TGraph`).

You run it like
```C++
//Generate a 2D plot to play with
.L chain.C+
TChain * t = chainit("/data/tdealtry/2021summerprod/sntools/v1.0.1/nakazato/NO_10kpc_2001/HyperK_20perCent/0000/wcsim/v1.9.4_C/triggerapp/v1.1.2_bonsaiv1.1.1_flowerv1.1_A/flbo/*.root");
t->Draw("Energy:Time", "", "COLZ");
auto htemp = (TH2F*)gPad->GetPrimitive("htemp"); // 2D

.L print_out_axis.C+
//check the x-axis binning
print_out_axis(htemp->GetXaxis());
//you can also check the y-axis
print_out_axis(htemp->GetYaxis());
```

## Histogram integrals
In the stats box on each plot you draw, you can see the integral of the histogram.
This is useful, but you sometimes need to 
- get the integral for a subset of the histogram, and/or
- have access to the integral programatically (e.g. if you want to plot the integral of different histograms)

Handily ROOT has a function to integrate for us, **if we know the bins of interest**
```C++
//Generate a 1D & 2D plots to play with
.L chain.C+
TChain * t = chainit("/data/tdealtry/2021summerprod/sntools/v1.0.1/nakazato/NO_10kpc_2001/HyperK_20perCent/0000/wcsim/v1.9.4_C/triggerapp/v1.1.2_bonsaiv1.1.1_flowerv1.1_A/flbo/*.root");

//1D
t->Draw("Time", "", "COLZ");
auto htemp = (TH1F*)gPad->GetPrimitive("htemp"); // 1D

// Print out the total integrals
std::cout << "Integral of 1D time histogram: " << htemp->Integral() << endl;

// If you're interested in only some bins, there's a function for that
std::cout << "Integrals of 1D time histogram:" << endl;
std::cout << " bins 5 to 10 inclusive: " << htemp->Integral(5, 10) << endl;
std::cout << " bin 3 only: " << htemp->Integral(3,3) << " should be equivalent to the bin content: " << htemp->GetBinContent(3) << endl;
std::cout << " bin 4 to the overflow " << htemp->Integral(4, htemp->GetNbinsX()+1) << endl;
std::cout << " the underflow to bin 2 " << htemp->Integral(0, 2) << endl;
std::cout << " the entire range excluding under-/over-flow: " << htemp->Integral(1, htemp->GetNbinsX()) << " should be equivalent to Integral() with no arguments: " << htemp->Integral() << endl;
```

And once you've finished playing with the 1D histogram, you can play with a 2D histogram
```
//2D
t->Draw("Energy:Time", "", "COLZ");
auto htemp2 = (TH2F*)gPad->GetPrimitive("htemp"); // 2D

// Print out the total integrals
std::cout << "Integral of 2D energy vs time histogram: " << htemp2->Integral() << endl;

//And integrate portions of the histogram
//Similar to 1D - you just need to give it both the x & y bin ranges you're interested in
std::cout << "Integrals of 2D energy vs time histogram:" << endl;
std::cout << " entire column 5 (neglecting under-/over-flow): " << htemp2->Integral(5, 5, 1, htemp2->GetNbinsY()) << endl;)
```

### `FindBin()`
Say we have a histogram of time, and we want to integrate from `t = 0` to `t = 1` second.
And say also that we don't know what the binning is.
We've then got 2 options to find the bins that contain 0 to 1 seconds
1. Loop over bins using `GetBinCenter()`, `GetBinLowEdge()` or `GetBinUpEdge()` (see above)
2. Use `FindBin()`

`FindBin()` returns the bin where the argument value(s) would reside

Using the same histograms as above
```C++
cout << "Bin number where value 0 resides: " << htemp->FindBin(0 + 1E-6) << endl;
cout << "Bin number where value 1E9 ns (1 second) resides: " << htemp->FindBin(1E9 - 1E-6) << endl;
```
Why have I added/subtracted a small number (1E-6)?
Think about floating point precision, and what happens if the value 0 is on a bin edge/boundary

We can then use the result of this to find the integral from 0 to 1 seconds
```C++
cout << "Integral from 0 to 1 seconds: " << htemp->Integral(htemp->FindBin(0 + 1E-6), htemp->FindBin(1E9 - 1E-6)) << endl;
```

#### 2D
For 2D, things are more complicated, as the function returns the *global* bin, rather than the x, y, and z bins.
So you need to convert the global bin to x, y, and z bins to use it in the integral.

```C++
// Find the bin that contains time=0 seconds, energy=10 MeV
int global_bin = htemp2->FindBin(0 + 1E-6, 10 + 1E-6);

// Create variables to store the x, y, z bin values
int x_bin, y_bin, z_bin;

//Get the x, y, z, bins
htemp2->GetBinXYZ(global_bin, x_bin, y_bin, z_bin);

std::cout << "time 0 s, energy 10 MeV corresponds to global bin: " << global_bin << " and x,y,z bins: " << x_bin << "," << y_bin << "," << z_bin << endl;
```


## Histogram projections
Say you have a 2D histogram of energy on the vertical axis against time along the horizontal axis.
And say you want to find the average energy as a function of time, and display the results in a 1D histogram.

One way to do it is by grabbing 1D **projections** of the 2D histogram, and getting the information from there.

**Note** another way to do this is with something like the `while` loop in `loop_over_time()` in `loop_over_time.C`

Here's a commented example of how to use histogram projections
```C++
//First, load a reconstructed TChain for an example SN
.L chain.C+
TChain * t = chainit("/data/tdealtry/2021summerprod/sntools/v1.0.1/nakazato/NO_10kpc_2001/HyperK_20perCent/0000/wcsim/v1.9.4_C/triggerapp/v1.1.2_bonsaiv1.1.1_flowerv1.1_A/flbo/*.root");

//I'm going to create a few canvases throughout the example,
// so we can see the result of each step
new TCanvas("c2d");
//Draw 2D histogram without cuts
// The COLZ is important, as it forces creation of a TH2F (rather than a TGraph)
t->Draw("Energy:Time", "", "COLZ");

//Get the 2D histogram
auto htemp = (TH2F*)gPad->GetPrimitive("htemp"); // 2D

//Create a 1D histogram to store the output in
// We can use the binning of the 2D histogram's x axis to 
// define the binning of the 1D histogram
auto hout = new TH1D("hout", ";Time (unit);Average energy (unit)",
	htemp->GetNbinsX(),
	htemp->GetXaxis()->GetBinLowEdge(1),
	htemp->GetXaxis()->GetBinUpEdge(htemp->GetNbinsX()));
	
//loop over x axis (time) bins
for(int ix = 1; ix <= htemp->GetNbinsX(); ix++) {
	//Get the 1D projection of this bin
	// This means for an x-axis bin (or range of bins), get the 1D y-axis distribution
	//  (in this case, energy)
	auto h1 = htemp->ProjectionY(TString::Format("_py%d", ix), ix, ix);
	
	//As a sanity check, ensure that the integral of this 1D projection is identical to
	// the column of bins in the original 2D histogram
	std::cout << h1->Integral() << "\t" << htemp->Integral(ix, ix, 0, htemp->GetNbinsY()+1) << std::endl;
	
	//Now we have the 1D histogram, grab the variables of interest
	double mean = h1->GetMean();
	
	//Add them to the output histogram, for this bin
	hout->SetBinContent(ix, mean);
	//Remember to calculate the appropriate bin error & set it using
	// hout->SetBinError(ix, error); 

	//Another sanity check - draw one random 1D projection to check how it looks
	// (and importantly that it is actually energy on the x-axis of the projection)
	if(ix == 15) {
		new TCanvas("hproj");
		h1->Draw();
	}
}//ix

//Draw the output histogram
new TCanvas("hout");
hout->Draw();
```

## `TGraph`s
So far, we've plotted histograms (e.g. `TH1F`, `TH2F`). Histograms require regular, pre-defined binning

Another useful tool are `TGraph`s, where you say point-by-point, where you want to put each point.

When might you like to use a `TGraph`? For example, when plotting a quantity (e.g. total number of events) as a function of simulated distance

```C++
.L chain.C+

//Setup the distances to loop over
vector<int> distances = {10, 20, 50, 60, 80, 100};
const int ndistances = distances.size();
//and use this to initialise the TGraph with the number of points we're interested in
TGraph * g = new TGraph(ndistances);

//Lets do a loop over distances
for(int idistance = 0; idistance < ndistances; idistance++) {
	int distance = distances[idistance];

	//At each distance, first load a reconstructed TChain for an example SN
	// We'll load the truth files, for a change
	TChain * t = chainit(TString::Format("/data/tdealtry/2021summerprod/sntools/v1.0.1/nakazato/NO_%dkpc_2001/HyperK_20perCent/0000/sntk/*.root", distance), "sntools");

	//The distance for the x-axis comes from the vector of distances
	int x = distance;
	
	//The number of entries on the y axis comes from the tree
	// TTree::GetEntries() has an option argument: a cut
	// (just like the 2nd argument of TTree::Draw())
	TString cut = TString::Format("distance==%d", distance);
	double y = t->GetEntries(cut);
	
	//Now we have x & y for this point, set the point on the graph
	g->SetPoint(idistance, x, y);
}//end loop over distances

//First lets make things obvious when plotted
// The marker (for more see https://root.cern.ch/doc/master/classTAttMarker.html)
g->SetMarkerStyle(20);
g->SetMarkerSize(3);
// The line (for more see https://root.cern.ch/doc/master/classTAttLine.html)
g->SetLineColor(kRed);

//TGraph::Draw() is similar, but no identical to drawing a histogram
// See https://root.cern.ch/doc/master/classTGraphPainter.html for all options
//The main thing to remember is that if you haven't drawn axes yet, you need to pass the "A" option
// L draws straight lines between points
// P draws the points
g->Draw("ALP");
```

### Drawing multiple `TGraph`s on the same axes
Recall that to plot multiple histograms on the same axes, you need to pass the drawing option `"SAME"`

You **don't** need to do that for `TGraph`s. Instead, you need to make sure not to include an `"A"` in the drawing option after the first one (otherwise a fresh set of axes will be drawn)

```C++
//Say you have a vector of graphs like this
vector<TGraph> graphs;

//do something to fill the graphs!

for(int igraph = 0; igraph < graphs.size(); igraph++) {
	// the ? is a true/false test for igraph.
	// If true (i.e. if igraph != 0), does the first thing (draws with "LP")
	// If false (i.e. if igraph == 0), does the second thing (draws with "ALP")
	graphs[igraph].Draw(igraph ? "LP" : "ALP");
} //igraph
```

Oh, and [don't forget about legends!](https://gitlab.com/tdealtry/snprojects/-/blob/master/Session4.md?ref_type=heads#plot-legends)

### Adding errors
Things are basically the same, but you just need to use a slightly different class, and call additional methods

#### Symmetric errors
- Use `TGraphErrors` instead of `TGraph`
- Call `g->SetPointError(ipoint, point_x_err, point_y_err);`
  - Note that `point_*_err` can be 0 (e.g. we know exactly the distance we simulated the SN at)

#### Asymetric errors
- Use `TGraphAsymmErrors` instead of `TGraph`
- Call `g->SetPointError(ipoint, point_x_lo_err, point_x_hi_err, point_y_lo_err, point_y_hi_err);`
  - Again any of the `point_*_*_err`s can be 0

## `TVector3` maths
[TVector3](https://root.cern.ch/doc/master/classTVector3.html) is a general three vector class. Useful for storing positions, directions, 3-momenta, etc.
The class comes in-built with a load of mathematical operations to make life easier!

### Dot product
```C++
double dot_prod = v1.Dot(v2); 
```

### Addition
Say you have a load of `TVector3`s and want to find the *average* of them.
Mathematically, I believe, the way to do this is to add them up (and I don't think you need to divide by the number of vectors - but do check).

Basically I think of this as lining all the vectors up from tip-end to non-tip-end (kind of like a random walk). The resulting vector gives the direction of travel and magnitude.

```C++
//say you have a vector of TVector3s
vector<TVector3> v3s;

//do something to set them!

//Create the "total" vector, ensuring it is initialised to 0,0,0
TVector3 total_v3(0,0,0);

//loop over the vector of TVector3s
for(auto v3 : v3s) {
	//increment the total vector based on the current vector
	total_v3 += v3;
}//end loop over v3s

//finally print the total vector
v3.Print();
```
