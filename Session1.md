# Session 1
[[_TOC_]]

## Summary of things

### Linux terminal
- Open a terminal from Applications/System Tools
- List files/directories in the current directory(folder) with `ls` (LiSt)
  - List files in a relative location with `ls other_folder`
  - List files in an absolute location with `ls /home/hyperk/tdealtry/snprojects`
- Navigate directories with `cd` (Change Directory)
  - Again can be relative like `cd other_folder`
  - Or absolute like `cd /home/hyperk/tdealtry/snprojects`
  - A couple of nice shortcuts
	- `cd -` to go to the previous directory you were in
	- `cd` to go to your home directory (e.g. `/home/hyperk/tdealtry/`)
- Press `<TAB>` all the time for auto-completion. If nothing happens, press it again and you'll get a list of options
- Use `<UP ARROW>` to scroll through previous commands, so you don't have to type long things again and again
- To copy & paste into the terminal
  - Highlight to copy
  - Middle click to paste
- `emacs` is a text editor, that has syntax highlighting and lots of powerful features
  - `gedit` is probably a better starting place, as (I believe) it has more Windows-like commands such as `<Ctrl>+s` to save
  - `vim` is a different choice in the other direction, being not intuitive at all (but very powerful)
  - Whichever text editor you choose, note that the recommended way to open the editor is with an `&` after the command. This puts the command into the *background*, which means you have access to both the terminal and the text editor. This means you can do the "edit code -> run code -> edit code -> ..." loop without having to open your editor every time
	- e.g. `gedit &` or `gedit filename &`
- For more, see Chapter 3 of the *New Starter Notes* pdf

### How to setup the HK software
- We added an alias to your terminal logon script (`.bashrc`) called `hksetup`
- This means if you type `hksetup` in a terminal, all the HK software will be automatically setup for you to use in **that** terminal

### The stages of HK software 
- See Chapter 8 in the *New Starter Notes* pdf
- The main file types you'll be working with are
  - sntools (true neutrino interactions in the detector)
  - flbo (reconstructed triggers)

### The types of files
#### sntools (sntk)
Contain true neutrino interactions in a `TTree` called `sntools`
| Variable | Description |
| :------- | :---------- |
| distance | Distance to the SN (kpc) |
| mass_ordering | Mass ordering (NO or IO = normal or inverted) |
| model | Model (nakazato or warren2020) |
| progenitor | Model-specific. Describes e.g. progenitor mass (e.g. 2001) |
| time | When in the SN flux this interaction occurred, relative to the model 0 time based on a SN explosion property (milliseconds) **WARNING** flbo uses nanoseconds |
| intmode | What interaction occurred (e.g. inverse beta decay, electron elastic scattering, ...) See below for explanation of values |
| position | Where in the detector the interaction happened? (cm) 3D: `[0] = x`, `[1] = y`, `[2] = z` |
| nu_pdg | [PDG code](https://pdg.lbl.gov/2007/reviews/montecarlorpp.pdf) for the neutrino that interacted |
| nu_totalenergy | Total energy of the neutrino (MeV) |
| nu_direction | Direction of the neutrino. A 3D unit vector |
| target_pdg | [PDG code](https://pdg.lbl.gov/2007/reviews/montecarlorpp.pdf) for the particle the neutrino interacted with |
| target_totalenergy | Total energy of the target (MeV) |
| target_direction | Direction of the target. A 3D unit vector |
| outgoing_pdg | [PDG code](https://pdg.lbl.gov/2007/reviews/montecarlorpp.pdf) for the (mostly) electron/positron coming from the interaction |
| outgoing_totalenergy | Total energy of the electron/positron (MeV) |
| outgoing_direction | Direction of the electron/positron. A 3D unit vector |
| outgoing_neutrons | Number of neutrons released from the interaction (should be e.g. 1 for IBD, 0 for elastic scattering) |

An explanation of the `intmode`
| intmode | Description |
| :------ | :---------- |
| -1001001 | Inverse beta decay: nuebar + p -> n + positron |
| (-)98 | Electron elastic scattering: nu(bar) + electron -> nu(bar) + electron |
| (-)2001001 | NC oxygen scattering: nu(bar) + proton -> nu(bar) + proton |
| (-)1008016 | CC oxygen scattering: nue(bar) + oxygen16 -> X + electron(positron) |
| (-)2006012 | NC carbon scattering: nu(bar) + carbon12 -> nu(bar)' + carbon12\*, carbon12\* -> carbon12 + gamma |
| (-)1006012 | CC carbon scattering: nue(bar) + carbon12 -> X + electron(positron) |

#### flbo (reconstructed files)
Contain reconstructed triggers in a `TTree` called `reconTree`

Warning: 1 neutrino interaction can appear in 2 `flbo` files!

This is because the full sntools file (up to 20 seconds) is split into 1 ms overlapping time chunks,
which are fed through WCSim & TriggerApplication. 
This is because 20 seconds is way too long to be able to simulate.
The overlapping is so we don't miss anything in the transition region, but will mean that we sometimes see things twice.
(Note that in the real detector, these overlapping chunks will exist in the triggering system)

| Variable | Description |
| :------- | :---------- |
| WCSimEntryNum | Ignore. Is a link to the WCSim file |
| WCSimEventNum | Ignore. Is a link to the WCSim file |
| TriggerNum | Ignore. Which trigger number in the file this corresponds to. Is another link to the WCSim-like trig file |
| NDigits | Number of "hits"/"digitised hits"/"digits" in the trigger. Roughly, how many PMTs have a signal in the trigger readout window |
| Energy | Reconstructed energy (MeV) |
| Reconstructer | Ignore. Basically a number saying which reconstruction algorithms were used. These file all use BONSAI (for position & direction) & FLOWER (for energy) |
| Time | Reconstructed time (nanoseconds) **WARNING** sntools uses milliseconds |
| Vertex | Reconstructed position (cm) 3D: `[0] = x`, `[1] = y`, `[2] = z` |
| GoodnessOfFit | Ignore to start. Some indication of how well the position fit converged to an answer |
| GoodnessOfTimeFit | Ignore to start. Some indication of how well the position fit converged to an answer |
| HasDirection | Ignore. All these files have position associated with them |
| DirectionEuler | Reconstructed direction as Euler angles`[0] = theta(zenith)`, `[1] = phi(azimuth)`, `[2] = alpha` |
| CherenkovCone | Reconstructed Cherenkov cone `[0] = cos(Cherenkov angle)`, `[1] = ellipticity` (Deviation from perfect circular or spherical form toward elliptic or ellipsoidal form), `[2] = likelihood` (Some indication of how well the Cherenkov cone fit converged to an answer) |
| DirectionLikelihood | Ignore to start. Some indication of how well the direction fit converged to an answer |

Note that if you look at the energy distribution for a dark noise-only event, it will look very quantised. I *think* this is due to the fact that the FLOWER algorithm has some quantised assumptions that combine with the random dark noise effect. This is all very hand wavy I know...

### The types of Supernova
- We have a few hundred simulated SN at Lancaster. These can be broken down into
  - Different models (Nakazato, Warren2020)
  - Different progenitors
	- For Nakazato, 1301 = 13 solar mass, 2001 = 20 solar mass
	- For Warren, we just have m10.5 = 10.5 solar mass
  - Different neutrino oscillation mass ordering (normal ordering = NO, inverted ordering = IO)
  - Different distances (10, 20, 50, 60, 80, 100 kpc)
  - And for each configuration (model, progenitor, mass ordering, distance) we have some number of repeats (10 to 100)
- All SN can be found at `/data/tdealtry/2021summerprod/sntools/`
  - There is a deeply nested folder structure as we go through each step in the simulation chain
  - The example flbo files we looked at were in `/data/tdealtry/2021summerprod/sntools/v1.0.1/nakazato/NO_100kpc_2001/HyperK_20perCent/0000/wcsim/v1.9.4_C/triggerapp/v1.1.2_bonsaiv1.1.1_flowerv1.1_A/flbo/`
  - The corresponding sntools files are in `/data/tdealtry/2021summerprod/sntools/v1.0.1/nakazato/NO_100kpc_2001/HyperK_20perCent/0000/sntk/`
  
#### SN locations
For reference, here's some tables of SN locations on the Lancaster cluster
##### Nakazato model, 5 kHz dark noise rate, ~20 seconds of flux simulated

- sntools version `v0.7.2`
- WCSim configuration `v1.9.4_A`
- TriggerApp configuration `v1.1.1_bonsaiv1.1.1_flowerv1.1_A`

For example at `/data/tdealtry/2021summerprod/sntools/v0.7.2/nakazato/NO_10kpc_2001/HyperK_20perCent/0009/wcsim/v1.9.4_A/triggerapp/v1.1.1_bonsaiv1.1.1_flowerv1.1_A/flbo/`

| Progenitor | Mass ordering | Distance | Number of SN simulated |
| :------- | :------- | :------- | :------- |
| **1301** | NO | 50 kpc | **100** |
| 2001 | NO | 10 kpc | 10 |
| 2001 | NO | 20 kpc | 10 |
| 2001 | NO | 50 kpc | **100** |
| 2001 | NO | 60 kpc | 10 |
| 2001 | NO | 100 kpc | 10 |
| 2001 | IO | 10 kpc | 10 |
| 2001 | IO | 20 kpc | 10 |
| 2001 | IO | 50 kpc | 10 |
| 2001 | IO | 60 kpc | 10 |
| 2001 | IO | 100 kpc | 10 |

##### Warren model, 5 kHz dark noise rate, ~1.5 seconds of flux simulated

- sntools version `v1.0.1`
- WCSim configuration `v1.9.4_B`
- TriggerApp configuration `v1.1.2_bonsaiv1.1.1_flowerv1.1_A`

For example at `/data/tdealtry/2021summerprod/sntools/v1.0.1/warren2020/NO_100kpc_a1.25_m10.5/HyperK_20perCent/0000/wcsim/v1.9.4_B/triggerapp/v1.1.2_bonsaiv1.1.1_flowerv1.1_A/flbo/`

| Progenitor | Mass ordering | Distance | Number of SN simulated |
| :------- | :------- | :------- | :------- |
| a1.25_m10.5 | NO | 10 kpc | 50 |
| a1.25_m10.5 | NO | 20 kpc | 50 |
| a1.25_m10.5 | NO | 50 kpc | 50 |
| a1.25_m10.5 | NO | 60 kpc | 50 |
| a1.25_m10.5 | NO | 100 kpc | 50 |
| a1.25_m10.5 | IO | 10 kpc | 50 |
| a1.25_m10.5 | IO | 20 kpc | 50 |
| a1.25_m10.5 | IO | 50 kpc | 50 |
| a1.25_m10.5 | IO | 60 kpc | 50 |
| a1.25_m10.5 | IO | 100 kpc | 50 |

###### No dark noise, different incoming neutrino directions
For this set of parameters, we also have repeats of the same underlying neutrino interactions, rotated to occur at different points on the sky. (Note that the default direction in all other SN is the z-direction `(0,0,1)`)

In the interest of speed, dark noise was turned off for the simulation.
Otherwise, simulation parameters are the same.

The directory structure of these simulations is different
- `/data/tdealtry/2021summerprod/sntools/v1.0.1/warren2020/DISTANCE_PROGENITOR/HyperK_20perCent/REPEATNUM/sntk/DIRECTION/*.root` is where the `sntk` true files live
- `/data/tdealtry/2021summerprod/sntools/v1.0.1/warren2020/DISTANCE_PROGENITOR/HyperK_20perCent/REPEATNUM/sntk/DIRECTION/wcsim/triggerapp/*flbo.root` is where the `flbo` reconstructed files live

where direction is one of
```
dir_0.000000_0.000000_1.000000
dir_0.258819_0.000000_0.965926
dir_0.500000_0.000000_0.866025
dir_0.707107_0.000000_0.707107
dir_0.866025_0.000000_0.500000
dir_0.965926_0.000000_0.258819
dir_1.000000_0.000000_0.000000
```
where the `x,y,z` direction unit vector is described in the directory name.
i.e. the SN is simulated at `+x`, `+z`, and 5 intermediate directions inbetween (cylindrical symmetry is assumed, hence why `y=0` for all simulations)

An example path is
- `sntk`: `/data/tdealtry/2021summerprod/sntools/v1.0.1/warren2020/NO_10kpc_a1.25_m10.5/HyperK_20perCent/0000/sntk/dir_0.000000_0.000000_1.000000/*.root`
- `flbo` : `/data/tdealtry/2021summerprod/sntools/v1.0.1/warren2020/NO_10kpc_a1.25_m10.5/HyperK_20perCent/0000/sntk/dir_0.000000_0.000000_1.000000/wcsim/triggerapp/*flbo.root`

There are a total of 3500 simulations
- 50 repeats per configuration
- x 5 distances
- x 2 mass orderings
- x 7 directions

##### Nakazato model, 10 kHz dark noise rate, ~1.5 second of flux simulated

- sntools version `v1.0.1`
- WCSim configuration `v1.9.4_C`
- TriggerApp configuration `v1.1.2_bonsaiv1.1.1_flowerv1.1_A`

For example at `/data/tdealtry/2021summerprod/sntools/v1.0.1/nakazato/NO_100kpc_2001/HyperK_20perCent/0000/wcsim/v1.9.4_C/triggerapp/v1.1.2_bonsaiv1.1.1_flowerv1.1_A/flbo/`

| Progenitor | Mass ordering | Distance | Number of SN simulated |
| :------- | :------- | :------- | :------- |
| 2001 | NO | 10 kpc | 50 |
| 2001 | NO | 20 kpc | 50 |
| 2001 | NO | 50 kpc | 50 |
| 2001 | NO | 60 kpc | 50 |
| 2001 | NO | **80** kpc | 50 |
| 2001 | NO | 100 kpc | 50 |

##### Nakazato model, 20 kHz dark noise rate, ~1.5 second of flux simulated

- sntools version `v1.0.1`
- WCSim configuration `v1.9.4_D`
- TriggerApp configuration `v1.1.2_bonsaiv1.1.1_flowerv1.1_A`

For example at `/data/tdealtry/2021summerprod/sntools/v1.0.1/nakazato/NO_100kpc_2001/HyperK_20perCent/0000/wcsim/v1.9.4_D/triggerapp/v1.1.2_bonsaiv1.1.1_flowerv1.1_A/flbo/`

| Progenitor | Mass ordering | Distance | Number of SN simulated |
| :------- | :------- | :------- | :------- |
| 2001 | NO | 10 kpc | 10 |
| 2001 | NO | 20 kpc | 10 |
| 2001 | NO | 50 kpc | 10 |
| 2001 | NO | 60 kpc | 10 |
| 2001 | NO | **80** kpc | 10 |
| 2001 | NO | 100 kpc | 10 |

##### Summary
| Model | Dark noise rate | Simulated time | WCSim configuration |
| :----- | :----- | :----- | :----- |
| Nakazato | 5 kHz | ~20 seconds | `v1.9.4_A` |
| Warren | 5 kHz | ~1.5 seconds | `v1.9.4_B` |
| Nakazato | 10 kHz | ~1.5 seconds | `v1.9.4_C` |
| Nakazato | 20 kHz | ~1.5 seconds | `v1.9.4_D` |

#### Progenitor definitions

##### Nakazato
| Progenitor tag | Initial mass `M_init` | Solar metalicity `Z` | Shock revival time `t_revive` |
| :--- | :--- | :--- | :--- |
| 1301 | 13 `M_sol` | 0.02 | 100 ms |
| 2001 | 20 `M_sol` | 0.02 | 100 ms |

##### Warren
| Progenitor tag | Turbulence strength parameter `alpha_lambda` | Initial mass `M_init` |
| :--- |  :--- |  :--- |
| a1.25_m10.5 | 1.25 | 10.5 |
| a1.25_m20.0 | 1.25 | 20.0 |

### ROOT
- ROOT is a data analysis framework, and provides a special file format (that the sntools & flbo files use)
  - We use it to read & write data, data analysis (including plotting histograms), etc.
- Open ROOT by typing `root` in a terminal
- Open a `TBrowser` to view the contents of a file
```C++
TBrowser b
```
  - If the file has a `TTree` then you can double click to draw simple (1D, no cut) histograms of the variables
  - If the file has histograms (e.g. `TH1D`), then you can double click to draw the saved histogram
- In order to produce more complicated plots, you need to use the ROOT command line. If your `TTree` is called `reconTree`
  - `reconTree->Draw("Energy")` to draw the 1D reconstructed energy distribution
  - `reconTree->Draw("Vertex[0]:Vertex[1]")` to draw the 2D reconstructed vertex position distribution for component 0 (= `x`) and component 1 (= `y`)
- Note that you can use `<TAB>`, `<UP ARROW>` etc. (just like a terminal) to help write your commands
- To quit `root`, type in the root command line
```C++
.q
```
  - If you're stuck in a bad state
    - Try `.qqqqqqqqq`
	- If that doesn't work, `<Ctrl>+c` a few times
	- If that doesn't work, `<Ctrl>+z`. This will put the root process into the background, and will say something like
	```
	[1]+  Stopped                 root -l
	```
	In order to really kill it, you need to type
	```bash
	kill %1
	```
	where you replace `1` here with the number in the square brackets from when you put root into the background
- For more, see chapter 7 of the *New Starter Notes* pdf
