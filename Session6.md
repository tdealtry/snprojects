# Session 6
[[_TOC_]]

## Saving multiple histograms to a pdf file
You're going to have lots & lots of plots. If you save 1 plot per PDF, you're going to have lots & lots of PDFs (that you need to name uniquely).

This is entirely personal preference, but it can be easier to save multiple plots in a single PDF

```C++
TCanvas * c = new TCanvas();
//Open the output PDF
c->SaveAs("out.pdf[");

//Plot & save your plot(s) of interest
t->Draw("Energy");
//Remembering to save your canvas after each one
c->SaveAs("out.pdf");
t->Draw("Time");
c->SaveAs("out.pdf");

//Save the output PDF. This is important!
// If you miss this, the output PDF will be corrupted
c->SaveAs("out.pdf]");
```
See `loop_over_time_savepdf.C` for a working example.

### Animated gif
If (for some reason...) you'd like an animated gif, you can do this too
```C++
TCanvas * c = new TCanvas();

//Plot & save your plot(s) of interest
t->Draw("Energy");
//Remembering to save your canvas after each one
c->SaveAs("out.gif+10");
t->Draw("Time");
c->SaveAs("out.gif+10");
```
The `10` in the argument says how many 10s of ms each image should be shown for (100 ms in this case)

### Aside: including in your LaTeX report
If you usually add plots to your report with
```latex
\includegraphics[width=5cm, height=4cm]{plot.pdf}
```
You can add a specific page of a pdf with e.g.
```latex
\includegraphics[width=5cm, height=4cm, page=3]{plot.pdf}
```

## Adding text to a plot
You already know how to put text in multiple places
- Axes titles
- Canvas titles
- Inside legends

It can sometimes be useful to put in extra text elsewhere. To do that, you can use [`TLatex`](https://root.cern.ch/doc/master/classTLatex.html) which allows you to use LaTeX-like commands
```C++
TLatex text;
//use pad fraction coordinates. You don't have to, but it's generally easier (it's certainly more consistent)
text.SetNDC(true);
//write some text.
// - 0.1 is the x position (roughly the left hand edge of your text)
// - 0.9 is the y position (roughly the bottom edge of your text, but some things like subscripts can drop below)
// - the string is the text you want to add
text.DrawLatex(0.1, 0.9, "SN #nu_{e}");
//you can write some more elsewhere if you need
text.DrawLatex(0.4, 0.92, "Nakazato model, 20 solar mass, normal ordering");
```

See `loop_over_time_savepdf.C` (again) for a working example.

## Statistical tests
It can be useful to determine whether two distributions come from the same underlying distribution.
You might do this with a chi-squared test, a delta log-likelihood method, a [Kolmogorov-Smirnov test](https://en.wikipedia.org/wiki/Kolmogorov%E2%80%93Smirnov_test), (or many others...).

For example, given a SN burst in HK, can we determine the model (+ progenitor) that best describes the data? Can we say how accurately it matches the best model? And how well it matches other models? Can we go further and reject some models?
See [this HK paper](https://arxiv.org/abs/2101.05269) for an example of this using a delta log-likelihood method.

Handily, ROOT has functions to perform a few of these tests. Make sure to read up about the methods (including in the ROOT documentation describing each function) to ensure that you're using the right test for the job, and that you're using the functions and interpreting the results correctly.

### Unbinned data
A KS-test should really be performed with unbinned data. Luckily, we can access data inside the `TTree` entry-by-entry (aka in an unbinned way) as we have done previously in `read_from_tree.C`.

See `ks_test_unbinned.C` for an example of this, comparing the time distribution of 2 SN.

(Note that the KS test is performed without any fiducial volume or energy cuts. There are comments in the code of where you'd want to add these)

What happens if you modify the code to perform a KS test between a single SN & itself? Is this what you would expect?

**WARNING** be careful about reusing the `vector`s returned by `get_sn_unbinned_data()`. It appears that `TMath::KolmogorovTest()` is somewhat destructive...

It appears that there are no log-likelihood methods functions in ROOT (but maybe I've just not searched hard enough...). If this is the case, and you need to perform something with log-likelihoods, you'll probably want to
- Use a function that is very similar to `get_sn_unbinned_data()` to get the data you require (it may be that you can simply reuse it)
- Define your own log-likelihood function
- Write a new function, based on `ks_test_unbinned()`, that calls your 2 new functions

### Binned data
Despite the fact that there is a [warning that the KS test should only be used for unbinned data](https://root.cern.ch/doc/master/classTH1.html#aeadcf087afe6ba203bcde124cfabbee4) there is a histogram method (class function) that allows you to perform a KS test using histograms (aka binned data)

See `ks_test_binned.C` for an example of this, comparing the time distribution of 2 SN.

(Note that, again, the KS test is performed without any fiducial volume or energy cuts. There are comments in the code of where you'd want to add these)

For a chi-squared test that compares two histograms, [see here](https://root.cern.ch/doc/master/classTH1.html#ab7d63c7c177ccbf879b5dc31f2311b27)
