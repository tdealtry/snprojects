# Session 3
[[_TOC_]]

## Last time
Last time we learnt how to
- Style 2D histograms e.g. with `t->Draw("Vertex[0]:Vertex[1]", "", "COLZ");`
- Apply cuts e.g. with `t->Draw("Vertex[0]:Vertex[1]", "Energy>7", "COLZ");`
- Drawing multiple histograms on the same axes with `"SAME"`
- Use ROOT macros to reuse code, and to draw histograms for a whole SN (rather than just a part of it from a single file)
```C++
.L ~/Downloads/chain.C+
TChain * t = chainit("/data/tdealtry/2021summerprod/sntools/v1.0.1/nakazato/NO_10kpc_2001/HyperK_20perCent/0000/wcsim/v1.9.4_C/triggerapp/v1.1.2_bonsaiv1.1.1_flowerv1.1_A/flbo/*.root")
t->Draw("Vertex[0]:Vertex[1]","Energy>7","COLZ");
```

## Plotting more complicated things on an axis
You don't need to just plot single tree variables like `Energy` on the axis. You can plot much more complicated things like `1/Energy` or `cos(Energy) * e^(Time)` or something else non-nonsensical.

A better example is `r`, the reconstructed radial position. This makes a lot of sense, since our detector is a cylinder, so cylindrical co-ordinates can be very useful

Recall that `r = sqrt(x^2 + y^2)` To do this you do
```C++
t->Draw("sqrt(Vertex[0]*Vertex[0] + Vertex[1]*Vertex[1])");
```
and why not make a 2D plot, showing the full `r-z` position reconstruction
```C++
t->Draw("sqrt(Vertex[0]*Vertex[0] + Vertex[1]*Vertex[1]):Vertex[2]","","COLZ");
```

### Reconstructed direction
Euler angles are weird. There's multiple conventions on how to convert to `x,y,z` unit vectors. See [this question](https://stackoverflow.com/questions/1568568/how-to-convert-euler-angles-to-directional-vector). But basically, it's just a combination of `cos` and `sin` of the Euler angles, in some combination with some `-` signs

We have 3 Euler angles,
- `[0]` is the zenith angle (runs from `0` to `+pi`)
- `[1]` is the azimuthal angle (runs from `-pi` to `+pi`)
- `[2]` is the alpha (runs from `-pi` to `+pi`)


Using the convention that BONSAI uses, we can create a `TTree::Draw()` command
```C++
.L chain.C+
TChain * t = chainit("/data/tdealtry/2021summerprod/sntools/v1.0.1/nakazato/NO_10kpc_2001/HyperK_20perCent/0000/wcsim/v1.9.4_C/triggerapp/v1.1.2_bonsaiv1.1.1_flowerv1.1_A/flbo/*.root");

//setup a canvas so we can see 3 1D plots side by side
TCanvas * c = new TCanvas();
c->Divide(1,3);

//Draw the direction vectors
c->cd(1);
t->Draw("cos(DirectionEuler[1]) * sin(DirectionEuler[0])"); //x component
c->cd(2);
t->Draw("sin(DirectionEuler[1]) * sin(DirectionEuler[0])"); //y component
c->cd(3);
t->Draw("cos(DirectionEuler[0])"); //z component
```
Which gives a uniform distribution across all angles. Is this what we expect?

**Aside** What happens if you apply an energy cut to the `TTree::Draw()` command?

Let's check against the true neutrino direction distribution (which is a unit vector) to make sure we've used the correct convention to do the conversion
(I recommend not looking at the code here, but having a go at plotting yourself)

This code assumes you're in the same session, and draws the true distributions on the `SAME` axes as the reconstructed distributions
```C++
TChain * ttrue = chainit("/data/tdealtry/2021summerprod/sntools/v1.0.1/nakazato/NO_10kpc_2001/HyperK_20perCent/0000/sntk/sn_0000-000000000_nakazato_NO_10kpc_2001.root","sntools");

c->cd(1);
ttrue->Draw("nu_direction[0]","","SAME");
c->cd(2);
ttrue->Draw("nu_direction[1]","","SAME");
c->cd(3);
ttrue->Draw("nu_direction[2]","","SAME");
```
Again we get big peaks (delta functions this time) at `(0,0,1)`. Success!

## ROOT macros
Almost all of the commands so far are designed to be entered directly into your ROOT browser.
And as we've discussed, this is fine for trying things out, but not for serious analysis
* No long term record of what you did
* Copy/paste is slow
  * And is even slower when you want to modify things

That's why ROOT macros are great. You write C++ code in them, that ROOT can compile & run.

How do you write a ROOT macro?
1. Add any relevant `#include`s to the start of the file
   * If you see compilation errors like `././spread_plot.C:8:3: error: unknown type name 'TCanvas' TCanvas * c = new TCanvas();`, then you're missing the `TCanvas.h` header, so need `#include "TCanvas.h"` (ROOT is fairly nice in that most classes have a header file called the same thing (with an added `.h`))
2. Create a function (named the same as your filename)
3. Populate the function with code

As a concrete example, here's the reconstructed direction code above, macro-ified
```C++
#include "chain.C"
#include "TCanvas.h"

void draw_direction()
{
	TChain * t = chainit("/data/tdealtry/2021summerprod/sntools/v1.0.1/nakazato/NO_10kpc_2001/HyperK_20perCent/0000/wcsim/v1.9.4_C/triggerapp/v1.1.2_bonsaiv1.1.1_flowerv1.1_A/flbo/*.root");

	//setup a canvas so we can see 3 1D plots side by side
	TCanvas * c = new TCanvas();
	c->Divide(1,3);

	//Draw the direction vectors
	c->cd(1);
	t->Draw("cos(DirectionEuler[1]) * cos(DirectionEuler[0])"); //x component
	c->cd(2);
	t->Draw("sin(DirectionEuler[1]) * cos(DirectionEuler[0])"); //y component
	c->cd(3);
	t->Draw("sin(DirectionEuler[0])"); //z component
}
```
To use
1. Create a file called `draw_direction.C`
2. Copy the above code into it & save it
3. Run like this
```
root draw_direction.C+
```

## Saving plots
It is useful to be able to save plots as PDFs (highly recommended over png & jpg - much better quality) automatically in ROOT macros (and in the command line) using `TCanvas::SaveAs()`

A small (command line) example
```C++
// create a canvas plots can be drawn in
TCanvas * c = new TCanvas();

//draw something
t->Draw("Energy");

//save the canvas
c->SaveAs("Energy.pdf");
```

## Getting the results from a `TTree::Draw()`
You're going to end up looking at hundreds of SN, to see the differences between them. Whilst you can make an animated gif of 2D plots, this isn't the best quantitative way to compare things. So how can you?

You can take numbers that describe the data in some way. This can involve fitting a function (e.g. a Gaussian, straight line, something more complex, ...) to your data, and extracting the fit parameters. It can also mean taking raw statistical numbers from a histogram (e.g. mean, standard deviation, min/max, quartiles, ...)

The first thing to do is to retrieve the results of a `TTree::Draw()`. You first call `TTree::Draw()` which creates a histogram in memory called `htemp`
```C++
t->Draw("Energy");
```
This histogram can be grabbed via
```C++
auto htemp = (TH1F*)gPad->GetPrimitive("htemp"); // 1D
auto htemp = (TH2F*)gPad->GetPrimitive("htemp"); // 2D
auto htemp = (TH3F*)gPad->GetPrimitive("htemp"); // 3D
```
And then, to get the mean of a distribution you can do
```C++
double mean = htemp->GetMean();
double sd = htemp->GetRMS(); //This function returns the Standard Deviation (Sigma) of the distribution not the Root Mean Square (RMS).
```

Aside: after getting the histogram like this, you can do things like changing the line colour (note that ROOT uses American English, so there is no "u")
```C++
htemp->SetLineColor(kRed);
htemp->Draw();
```
And you can also change the axis labels, making it a more descriptive (and with units!)
```C++
htemp->GetXaxis()->SetTitle("Reconstructed energy (MeV)");
htemp->Draw();
```

### Getting `htemp` multiple times
If you do the following
```C++
t->Draw("Energy");
auto htemp = (TH1F*)gPad->GetPrimitive("htemp"); // 1D
htemp->GetEntries();
t->Draw("Time");
auto htemp = (TH1F*)gPad->GetPrimitive("htemp"); // 1D
htemp->GetEntries();
t->Draw("Time:Energy");
auto htemp = (TH2F*)gPad->GetPrimitive("htemp"); // 2D
htemp->GetEntries();
```
you'll get an error about redefinition of `htemp`. Let me explain why. And how to fix it

In C++, you need to define a variable once and only once. And the type should be given in the definition. In this example it is this line
```C++
auto htemp = (TH1F*)gPad->GetPrimitive("htemp"); // 1D
```
where the `auto` keyword is a cheat, that sets the type of the variable based on context (here to a `TH1F*`, or a pointer to a 1D histogram)

The first error comes from the second version of that line i.e.
```C++
auto htemp = (TH1F*)gPad->GetPrimitive("htemp"); // 1D
```
to get around it you can either use a different variable name each time (instead of `htemp`) or just drop the word `auto`
```C++
htemp = (TH1F*)gPad->GetPrimitive("htemp"); // 1D
```
This latter method works because you can redefine the value of a variable in C++ (in this case, you can modify `htemp` to point to a different histogram).

The second error comes from the line
```C++
auto htemp = (TH2F*)gPad->GetPrimitive("htemp"); // 2D
```
If you removed the word `auto`, it still wouldn't work, since `htemp` has already been defined as a `TH1F*`, rather than a `TH2F*` (due to the order of the lines above).
In order for it to work, you need to create a new variable e.g.
```C++
auto htemp2 = (TH2F*)gPad->GetPrimitive("htemp"); // 2D
```
and remember to use the 2D variable
```C++
htemp2->GetEntries()
```

### Scope
Be careful about scope!
```C++
t->Draw("Energy");
auto htemp = (TH1F*)gPad->GetPrimitive("htemp"); // 1D
htemp->GetEntries(); //This works
t->Draw("Time");
htemp->GetEntries(); //The original htemp has been deleted, so this causes a segmentation fault
```

Instead do this, if you need to hold multiple `TTree::Draw()` results at once
```C++
t->Draw("Energy")
auto htemp = (TH1F*)gPad->GetPrimitive("htemp"); //1D
htemp->GetEntries(); //This works
//Let's make a copy of htemp
auto henergy_nocuts = new TH1F(*htemp)
henergy_nocuts->GetEntries(); //This works
t->Draw("Time");
henergy_nocuts->GetEntries(); //This still works
```

## Comparing many histograms I (plotting many of the same histogram)
The first way to compare histograms from multiple SN is to simply plot them. This is a nice method when you are comparing the same thing (i.e. statistical repeats of the same SN) but gets too complicated for also comparing with other SN models/progenitors.

Here's a quick example
```C++
.L chain.C+
for(int iiter = 0; iiter < 50; iiter++){ 
	//generate the file path
	// the %04d is replaced by an int (%d), padded with 0s to 4 digits (04), to match the actual directory structure
	auto reco_file_path = TString::Format("/data/tdealtry/2021summerprod/sntools/v1.0.1/nakazato/NO_10kpc_2001/HyperK_20perCent/%04d/wcsim/v1.9.4_C/triggerapp/v1.1.2_bonsaiv1.1.1_flowerv1.1_A/flbo/", iiter);
	TChain * t = chainit(reco_file_path + "*.root", "reconTree");
	t->Draw("Energy","", iiter ? "SAME" : "");
}//iiter
```
Note the weird `iiter ? "SAME" : ""` is a logical expression. If `iiter` evaluates true (i.e. non-zero), it uses `"SAME"`. If `iiter` evaluates false (i.e. zero), it uses `""`

What do you think of the plot? Is it too busy? Are 50 lines too many? Are more histogram bins required?

A way to zoom in is via cuts e.g.
```C++
t->Draw("Energy", "Energy>10&&Energy<50", iiter ? "SAME" : "");
```

## Comparing many histograms II (plotting the spread from many histograms)
If you have lots of overlapping lines from method I, instead plotting the mean and spread (min/max and/or standard deviation) for each bin can be a nicer.
It can also be useful for comparing (a few) different SN configurations.

This is a little more complicated, so take a look in `spread.C`, and run with
```C++
root spread.C+
```
or alternatively
```C++
.L spread.C+
spread()
```

* Which SN have been plotted? How many?
* What do the error bars mean?
* Can you change the binning?
* What output files have been produced?

### ROOT macro arguments
`spread()` contains a lot of arguments. They all have default arguments (which is why you can run `spread()` without passing any arguments). But what if you want to change the value of an argument?

In C++ you need to give all arguments up to and including the last argument you want to modify. So say you want to plot time from 0 to 1 second (instead of energy from 10 to 50 MeV), you'd do
```C++
.L spread.C+
spread(20, 0, 1E9, "Time");
```
(I recommend using the `.L` method, rather than other methods, when you're using arguments with ROOT macros)

## Comparing many histograms III (plotting summary information from different SN)
Another way to compare the results of many SN
- With the repeats of the same configuration to show the statistical spread
- And also with different configurations to show how things are different for different configurations

is with two things
1. Create a place to store the result of each histogram's parameter(s) of interest (e.g. mean energy)
2. Run the code on multiple SN

### Creating a place to store histogram results
There's multiple ways of doing this (e.g. `std::vectors`, histograms (`TH1F`, `TH2F`, etc.), graphs (`TGraphErrors`, etc.)) but I'm going to recommend using a `TTree`
- It's quick to apply different cuts, create new histograms with different combinations of variables, etc.
- You've already learnt how to draw histograms from a `TTree`

### Running on many SN
Use `for` loops!

### Example analysis script
From the command line run
```bash
root -b -q analysis.C+
```
Then list the directory you ran from. What new files are there? What's in those files?

Can you plot the mean reconstructed energy distribution for the SN that were run?

Looking into `analysis.C`. Which SN have been run?

Can you plot & compare the mean reconstructed energy distribution for multiple distances? (To do this, you'll want to run with `root -b -q analysis.C'(-1)'` such that you run over all Nakazato 10 kHz files)

## Next step
The next step (already hinted at above) is to compare SN from different configurations. E.g. to see how the mean reconstructed energy distribution changes with distance
